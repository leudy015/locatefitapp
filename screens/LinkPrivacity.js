'use strict';
var React = require('react');
var PropTypes = require('prop-types');
var ReactNative = require('react-native');
var {Linking, StyleSheet, Text, TouchableOpacity, View, ScrollView} = ReactNative;

class OpenURLButton extends React.Component {
  static propTypes = {
    url: PropTypes.string,
  };

  handleClick = () => {
    Linking.canOpenURL(this.props.url).then(supported => {
      if (supported) {
        Linking.openURL(this.props.url);
      } else {
        console.log("Don't know how to open URI: " + this.props.url);
      }
    });
  };

  render() {
    return (
      <TouchableOpacity onPress={this.handleClick}>
        <View style={styles.button}>
          <Text style={styles.text}>Abrir {this.props.url}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

class IntentAndroidExample extends React.Component {
  static title = 'Linking';
  static description = 'Shows how to use Linking to open URLs.';

  render() {
    const {navigation} = this.props
    return (
      <View style={styles.container}>    
        <ScrollView
            showsHorizontalScrollIndicator={false}>
            <View style={styles.body}>
              <OpenURLButton url={'https://www.locatefit.es'} />
              <OpenURLButton url={'https://about.locatefit.es/'} />
              <OpenURLButton url={'https://about.locatefit.es/contacto'} />
              <OpenURLButton url={'https://about.locatefit.es/privacidad'} />
              <OpenURLButton url={'https://about.locatefit.es/condiciones-de-uso'} />
              <OpenURLButton url={'https://about.locatefit.es/cookies'} />
              <OpenURLButton url={'https://about.locatefit.es/preguntas-frecuentes'} />
              <OpenURLButton url={'https://about.locatefit.es/empleo'} />
            </View>
        </ScrollView>
        </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },

  body:{
    backgroundColor: 'transparent',
    padding: 10,
    paddingTop: 30,
  },
  button: {
    padding: 10,
    backgroundColor: '#fff',
    marginBottom: 10, 
    borderWidth: 0.5,
    borderColor: '#ddd'
  },
  text: {
    color: '#555',
  },
});

module.exports = IntentAndroidExample;