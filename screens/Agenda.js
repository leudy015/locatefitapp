import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { CalendarList } from 'react-native-calendars';

const horas = [
    {
        hora: '10:00'
    },
    {
        hora: '11:00'
    },
    {
        hora: '12:00'
    },
    {
        hora: '13:00'
    },
    {
        hora: '14:00'
    },
    {
        hora: '15:00'
    },
    {
        hora: '16:00'
    },
    {
        hora: '17:00'
    }
]

export default class Registration extends Component {

    render() {
        const { navigation } = this.props;

        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <ScrollView>
                        <View style={{ height: '50%' }}>
                            <CalendarList
                                // Enable horizontal scrolling, default = false
                                horizontal={true}
                                // Enable paging on horizontal, default = false
                                // Set custom calendarWidth.
                                calendarWidth={400}

                            />
                        </View>



                        <View style={{ width: '100%', paddingBottom: 10, marginTop: 10, alignItems: 'center' }}>
                            <View style={{ height: 150, marginTop: 20, textAlign: "left", }}>
                                <Text style={{ fontSize: 18, color: '#999', marginLeft: 20 }}>Horas disponibles para este día</Text>

                                <View style={{ marginTop: 20, width: '100%', flexDirection: 'row' }}>
                                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                        {
                                            horas.map( (time, i) =>{
                                                return(
                                                    <TouchableOpacity key={i} style={{ color: '#fff', backgroundColor: '#95ca3e', padding: 10, borderRadius: 10, width: 100, marginLeft: 20 }}>
                                                        <Text  style={{ fontSize: 18, color: 'white', textAlign: 'center' }}>{time.hora}</Text>
                                                    </TouchableOpacity>
                                                )
                                            }

                                            )
                                        }
                                    </ScrollView>
                                </View>
                            </View>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Payment')}
                                style={{
                                    width: "60%",
                                    height: 50,
                                    backgroundColor: '#95ca3e',
                                    borderRadius: 50,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    marginVertical: 10,
                                }}
                            >
                                <Text style={{ fontSize: 16, alignItems: "center", color: '#FFFFFF', justifyContent: "center", }}>Continuar</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    body: {
        width: "100%",
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 70
    },

});