import React from "react";
import {
  StyleSheet,
  ImageBackground,
  Dimensions,
  StatusBar,
  KeyboardAvoidingView,
  Alert
} from "react-native";
import { Block, Checkbox, Text, theme } from "galio-framework";
import { NETWORK_INTERFACE_LINK } from '../constants/config';
import { Button, Icon, Input } from "../components";
import { Images, argonTheme } from "../constants";
import axios from 'react-native-axios';

const { width, height } = Dimensions.get("screen");

class Register extends React.Component {
  state = {
    email: '',
    noEmail: false,
   };

  actualizarState = (name, value) => {
    this.setState({
      [name]: value
    });
  };

  handleSubmit = () => {
        const {email} = this.state;
        const {navigation} = this.props;
        const url =  NETWORK_INTERFACE_LINK + '/forgotpassword';
        if (email==''){
          Alert.alert(
            'Ohhh algo va mal ',
            'por favor escribe tu email para continuar',
            [
              { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false },
            )
            return null
        }  
        axios
          .post(url, {email})
          .then(res => {
           if (res.data.noEmail) {
              this.setState({
                noEmail: res.data.noEmail,
                message: res.data.message,
                email: ''
              });

              Alert.alert(
                'Error con tu Email',
                this.state.message,
                [
                  {
                    text: 'Regístrarme',
                    onPress: () => navigation.navigate('Account'),
                    style: 'cancel',
                  },
                  {
                    text: 'Volver a intentarlo',
                    onPress: () => console.log('Volver a intentar'),
                  },
                ],
              );

            } else {
              this.setState({
                email:''
              })
                navigation.navigate('Login')
            }
          })
          .catch(err => {
            console.log('error:', err);
          });
  };
  render() {
    const {email} = this.state;
    const {navigation} = this.props;
   
      return (
      <Block flex middle>
        <StatusBar hidden />
        <ImageBackground
          source={Images.RegisterBackground}
          style={{ width, height, zIndex: 1 }}
        >
          <Block flex middle>
            <Block style={styles.registerContainer}>
              <Block flex={0.25} middle style={styles.socialConnect}>
                <Text color="#8898AA" size={12}>
                  Recuperar contraseña
                </Text>
              </Block>
              <Block flex>
                <Block flex={0.17} middle>
                  <Text color="#8898AA" size={12}>
                    Escribe tu correo electrónico y te enviaremos un enlace
                  </Text>
                </Block>
                <Block flex center>
                  <KeyboardAvoidingView
                    style={{ flex: 1 }}
                    behavior="padding"
                    enabled
                  >

                    <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                    <Input
                        name='email'
                        onChangeText={value => this.actualizarState('email', value)}
                        value={email}
                        borderless
                        placeholder="Correo electrónico"
                        iconContent={
                          <Icon
                            size={16}
                            color={argonTheme.COLORS.ICON}
                            name="ic_mail_24px"
                            family="ArgonExtra"
                            style={styles.inputIcons}
                          />
                        }
                      />
                    </Block>
                    <Block middle>
                      <Button onPress={this.handleSubmit} color="primary" style={styles.createButton}>
                        <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                          ENVIADME EL ENLACE
                        </Text>
                      </Button>
                    </Block>
                    <Block center row width={width * 0.75}>
                      <Text bold size={14} color={argonTheme.COLORS.BLACK}>
                          ¿Aún no tienes una cuenta?
                      </Text>
                      <Button
                        onPress={() => navigation.navigate("Account")}
                        style={{ width: 150 }}
                        color="transparent"
                        textStyle={{
                          color: argonTheme.COLORS.PRIMARY,
                          fontSize: 14
                        }}
                      >
                        REGÍSTRARME
                      </Button>
                    </Block>
                  </KeyboardAvoidingView>
                </Block>
              </Block>
            </Block>
          </Block>
        </ImageBackground>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  registerContainer: {
    width: width * 0.9,
    height: height * 0.78,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },
  socialConnect: {
    backgroundColor: argonTheme.COLORS.WHITE,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: "#8898AA"
  },
  socialButtons: {
    width: 120,
    height: 40,
    backgroundColor: "#fff",
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1
  },
  socialTextButtons: {
    color: argonTheme.COLORS.PRIMARY,
    fontWeight: "800",
    fontSize: 14
  },
  inputIcons: {
    marginRight: 12
  },
  passwordCheck: {
    paddingLeft: 15,
    paddingTop: 13,
    paddingBottom: 30
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25,
    marginBottom: 25
  }
});

export default Register;
