import React, { Component } from 'react';
import {
    ScrollView,
    StyleSheet,
    Dimensions,
    ImageBackground,
  } from 'react-native';
import { Block, theme, Text } from 'galio-framework';
import { argonTheme, Images } from "../constants/";
import { Button, Icon, Input } from "../components";
import CardPro from '../components/CardPro';
import CardProVertical from '../components/CardProVertical';
import NavigationServices from '../navigation/Services'

const { width } = Dimensions.get('screen');


class Home extends Component{
    render(){
        return(
            <Block flex>
          <Block row flex>
            
            <Text
              h5
              style={{ marginBottom: theme.SIZES.BASE / 2 }}
              color={argonTheme.COLORS.DEFAULT}
            >
              Destacados.
          </Text>
            <Button
              onPress={() => NavigationServices.navigate("Searchscreen")}
              style={{ felxDireccion: 'start_end', marginRight: 0, marginLeft: 30 }}
              color="transparent"
              textStyle={{
                color: argonTheme.COLORS.PRIMARY,
                fontSize: 14
              }}
            >
              Ver todos
            </Button>
          </Block>

          <Block flex row>
            <ScrollView showsHorizontalScrollIndicator={false}>
              <CardPro style={{ marginRight: theme.SIZES.BASE }} />
            </ScrollView>
          </Block>
          <Block style={{marginBottom: 30, marginTop: 30}}>
          <Block flex card shadow style={styles.category}>
              <ImageBackground
                source={{ uri: Images.Products["View article"] }}
                style={[
                  styles.imageBlock,
                  { width: width - theme.SIZES.BASE * 2, height: 252 }
                ]}
                imageStyle={{
                  width: width - theme.SIZES.BASE * 2,
                  height: 252
                }}
              >
                <Block style={styles.categoryTitle}>
                  <Text size={18} bold color={theme.COLORS.WHITE}>
                    Servicios de organización de espacios
                  </Text>
                </Block>
              </ImageBackground>
            </Block>
          </Block>
          <Block row flex>
            <Text
              h5
              style={{ marginBottom: theme.SIZES.BASE / 3 }}
              color={argonTheme.COLORS.DEFAULT}
            >
              Lo más cerca de ti.
          </Text>
            <Button
              onPress={() => NavigationServices.navigate("Searchscreen")}
              style={{ felxDireccion: 'start_end', marginLeft: -30 }}
              color="transparent"
              textStyle={{
                color: argonTheme.COLORS.PRIMARY,
                fontSize: 14
              }}
            >
              Ver todos
            </Button>
          </Block>
          <CardProVertical horizontal />
        </Block>
        )
    }
}


const styles = StyleSheet.create({
    home: {
      width: width,
      justifyContent: 'center'
    },
    articles: {
      width: width - theme.SIZES.BASE * 2,
      paddingVertical: theme.SIZES.BASE,
    },
    categoryTitle: {
      height: "100%",
      paddingHorizontal: theme.SIZES.BASE,
      backgroundColor: "rgba(0, 0, 0, 0.3)",
      justifyContent: "center",
      alignItems: "center"
    },
  
    imageBlock: {
      overflow: "hidden",
      borderRadius: 4
    },
  });

export default Home;