import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, ActivityIndicator, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Block, theme, Text } from 'galio-framework';
import { Button } from '../components';
import { Progress } from '@ant-design/react-native';
import Icon from '../components/Icon';
import { argonTheme } from "../constants/";
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import OpenURLButton from '../components/Linkout';
const { width } = Dimensions.get('screen');

const PROFESSIONAL_ORDEN_PROCEED = gql`
  mutation ordenProceed($ordenId: ID!, $estado: String!, $progreso: String!, $status: String!, $nota: String) {
    ordenProceed(ordenId: $ordenId, estado: $estado, progreso: $progreso, status: $status, nota: $nota) {
      success
      message
    }
  }
`;


export default class OrderDetail extends Component {

handleAcepta(e, ordenProceed, data){
  e.preventDefault();
      this.setState();
      const formData = {
        ordenId: data.id,
        estado: 'Aceptado',
        progreso: '75',
        status: 'active',
      }
      ordenProceed({ variables: formData }).then(res => {
        if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
          this.setState({ visible: false, formLoading: false });
          console.log(res.data.ordenProceed.message);
          if (this.refetch) {
            this.refetch().then(res => {
              if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
                this.setState({ ordenes: res.data.getOrdenesByProfessional.list });
              }
            });
          }
        }
      }).catch(err => {
        this.setState({ visible: false, formLoading: false });
        console.log('Algo salió mal. Por favor intente nuevamente en un momento.')
      })
}

  handleCancelar(e, ordenProceed, data) {
    e.preventDefault();
    const that = this;
    return new Promise((resolve, reject) => {
      const formData = {
        ordenId: data.id,
        estado: 'Rechazado',
        progreso: '0',
        status: 'exception',
      }
      ordenProceed({ variables: formData }).then(res => {
        if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
          setTimeout(() => {
            console.log(res.data.ordenProceed.message);
            if (that.refetch) {
              that.refetch().then(res => {
                if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
                  that.setState({ ordenes: res.data.getOrdenesByProfessional.list });
                }

              });
            }
          }, 300)
          resolve();
        }
      }).catch(err => {
        setTimeout(() => {
          console.log('Algo salió mal. Por favor intente nuevamente en un momento.')
        }, 300)
      })
    })
  }

  finalizar(e, ordenProceed, data) {
    e.preventDefault();

    const formData = {
      ordenId: data.id,
      estado: 'Finalizada',
      progreso: '100',
      status: 'success',
    }
    ordenProceed({ variables: formData }).then(res => {
      if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
        console.log('La orden ha sido marcada como finalizada éxitosamente');
        if (this.refetch) {
          this.refetch().then(res => {
            if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
              this.setState({ ordenes: res.data.getOrdenesByProfessional.list });
            }
          });
        }
      }
    }).catch(err => {
      console.log('Algo salió mal. Por favor intente nuevamente en un momento.')
    })

  }


  _renderItem(item) {
    const { navigation } = this.props;
      const description = item.nota;
      const ubicacion = item.client.ciudad;
      const titulo = item.product.title;
      const date = new Date(item.endDate + ' ' + item.time);
      const year = date.getFullYear();
      // const year = date.getUTCFullYear();
      let month = date.getMonth() + 1;
      // let month = date.getUTCMonth() + 1;
      month = (month < 10) ? '0' + month : month;
      const day = date.getDate();
      // const day = date.getUTCDate();
      let hours = date.getHours();
      // let hours = date.getUTCHours();
      hours = (hours < 10) ? '0' + hours : hours;
      const minutes = date.getMinutes();
      // const minutes = date.getUTCMinutes();

      const startDateString = year + '' + month + '' + day + 'T' + hours + '' + minutes + '000';

      const productoTime = item.product.time.split(':');
      let endHours = Number(hours) + (Number(productoTime[0]) * Number(item.cantidad));
      let endMinutes = Number(minutes) + (Number(productoTime[1]) * Number(item.cantidad));
      if (endMinutes > 60) {
        endHours++;
        endMinutes -= 60;
      }

      endHours = (endHours < 10) ? '0' + endHours : endHours;
      endMinutes = (endMinutes < 10) ? '0' + endMinutes : endMinutes;
      const endDateString = year + '' + month + '' + day + 'T' + endHours + '' + endMinutes + '00';

      link = "https://calendar.google.com/calendar/r/eventedit?text=" + titulo + "&dates=" + startDateString +  "/" + endDateString + "&details=" + description + "&location=" + ubicacion + "&sprop=name:Locatefit&sf=true";

    return (
      <Block>
        <ScrollView showsHorizontalScrollIndicator={false} style={styles.container}>
          <Block flex >
            {item.product.fileList.length > 0 ?
              <Image source={{ uri: item.product.fileList.length > 0 ? "https://server.locatefit.es/assets/images/" + item.product.fileList[0] : "" }} style={{ width: '100%', height: 300 }} /> : null}
            <Icon family="Font-Awesome" name="times" style={{ position: 'absolute', marginTop: 40, marginLeft: 20 }} size={20} color="#95ca3e" onPress={() => navigation.navigate('Orders')} />
          </Block>

          <Block flex style={styles.cards}>
            <Text
              h5
              style={{ marginBottom: theme.SIZES.BASE / 2, marginTop: 15, marginLeft: 15 }}
              color={argonTheme.COLORS.DEFAULT}>
              {item.product.title}
            </Text>
            <Block row>
              <Text
                style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 15 }}
                color={argonTheme.COLORS.PLACEHOLDER}>
                {item.cantidad + item.product.currency}
              </Text>
              <Text size={12} style={styles.estados}>{item.estado}</Text>
            </Block>
          </Block>
          <Block flex style={styles.cards}>
            <Text
              p
              style={{ marginBottom: theme.SIZES.BASE / 2, marginTop: 15, marginLeft: 15, marginBottom: 20 }}
              color={argonTheme.COLORS.MUTED}>
              Fecha del realización del servicio:
            </Text>

            <Text
              style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 15 }}
              color={argonTheme.COLORS.PLACEHOLDER}>
              {item.endDate} a las {item.time}
            </Text>

            <Block center style={{ marginTop: 15, marginBottom: 15 }}>
              <OpenURLButton color='primary' texto='AÑADIR A GOOGLE CALENDAR' url={link} />
            </Block>
          </Block>
          <Block flex style={styles.cards}>
            <Text
              p
              style={{ marginBottom: theme.SIZES.BASE / 2, marginTop: 15, marginLeft: 15, marginBottom: 20 }}
              color={argonTheme.COLORS.MUTED}>
              Informacón del cliente:
            </Text>
            <Text
              style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 15 }}
              color={argonTheme.COLORS.DEFAULT}>
              {item.client.nombre}
            </Text>

            <Text
              style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 15 }}
              color={argonTheme.COLORS.PLACEHOLDER}>
              {item.client.calle}
            </Text>

            <Text
              style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 15 }}
              color={argonTheme.COLORS.PLACEHOLDER}>
              {item.client.ciudad},  {item.client.provincia}
            </Text>
            <Text
              style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 15 }}
              color={argonTheme.COLORS.PLACEHOLDER}>
              {item.client.codigopostal}
            </Text>
            <Text
              style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 15 }}
              color={argonTheme.COLORS.PLACEHOLDER}>
              {item.client.telefono}
            </Text>
          </Block>

          <Block flex style={styles.cards}>
            <Text
              p
              style={{ marginBottom: theme.SIZES.BASE / 2, marginTop: 15, marginLeft: 15, marginBottom: 20 }}
              color={argonTheme.COLORS.MUTED}>
              Detalles del Pedido:
            </Text>
            <Block row style={styles.detail}>
              <Text
                style={{ marginBottom: theme.SIZES.BASE / 2 }}
                color={argonTheme.COLORS.DEFAULT}>
                Número de Pedido:
              </Text>
              <Text
                style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 'auto' }}
                color={argonTheme.COLORS.MUTED}>
                {item.id}
              </Text>
            </Block>
            <Block row style={styles.detail}>
              <Text
                style={{ marginBottom: theme.SIZES.BASE / 2 }}
                color={argonTheme.COLORS.DEFAULT}>
                Fecha:
          </Text>
              <Text
                style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 'auto' }}
                color={argonTheme.COLORS.MUTED}>
                {item.created_at}
              </Text>
            </Block>
            <Block row style={styles.detail}>
              <Text
                style={{ marginBottom: theme.SIZES.BASE / 2 }}
                color={argonTheme.COLORS.DEFAULT}>
                Total:
          </Text>
              <Text
                style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 'auto', }}
                color={argonTheme.COLORS.MUTED}>
                {item.cantidad * item.product.number}€
          </Text>
            </Block>
          </Block>

          <Block flex style={styles.cards}>
            <Text
              p
              style={{ marginBottom: theme.SIZES.BASE / 2, marginTop: 15, marginLeft: 15, marginBottom: 20 }}
              color={argonTheme.COLORS.MUTED}>
              Nota del cliente:
            </Text>

            <Text
              style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 15 }}
              color={argonTheme.COLORS.PLACEHOLDER}>
              {item.nota}
            </Text>
          </Block>
          <Block style={styles.footer}>
            <Block center>
              <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
                {(ordenProceed) => (
                  <Button onPress={e => this.handleAcepta(e, ordenProceed, item)} color="default" style={styles.button}>
                    ACEPTAR ORDEN
                  </Button>)}
              </Mutation>
            </Block>

            <Block center>
              
                <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
                  {(ordenProceed) => (
                    <Button onPress={e => this.handleCancelar(e, ordenProceed, item)} color="error" style={styles.button}>
                      RECHAZAR ORDEN
                    </Button>)}
                </Mutation> 
            </Block>

            <Block center>
              <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
                {(ordenProceed) => (
                  <Button onPress={e => this.finalizar(e, ordenProceed, item)} color="primary" style={styles.button}>
                    FINALIZAR ORDEN
              </Button>)}
              </Mutation>
            </Block>
          </Block>
        </ScrollView>

      </Block>
    )
  }

  render() {
    const { navigation } = this.props;
    console.log(navigation)
    const data = navigation.getParam('data');
    return this._renderItem(data);
  }
}

const styles = StyleSheet.create({

  container: {
    marginBottom: 0
  },

  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
  optionsButton: {
    width: "auto",
    height: 34,
    paddingHorizontal: theme.SIZES.BASE,
    paddingVertical: 10,
    marginTop: 20,
    marginLeft: 'auto',
  },

  cards: {
    borderBottomColor: argonTheme.COLORS.BORDER_COLOR,
    borderBottomWidth: 0.5,


  },
  detail: {
    borderBottomColor: argonTheme.COLORS.BORDER_COLOR,
    borderBottomWidth: 0.5,
    padding: 15
  },

  footer: {
    marginTop: 30,

  },
  button: {
    marginBottom: theme.SIZES.BASE,
    width: width - theme.SIZES.BASE * 2
  },
  estados: {
    marginLeft: 'auto',
    padding: 5,
    backgroundColor: argonTheme.COLORS.INPUT_SUCCESS,
    color: argonTheme.COLORS.WHITE,
    borderRadius: 15,
    marginRight: 15,
    marginBottom: 10
  },
});