import React, { Component } from 'react';
import { StyleSheet, Image, Dimensions, ScrollView, ActivityIndicator } from 'react-native';
import { theme, Block, Text } from 'galio-framework';
import { Images, argonTheme } from "../constants";
import CardOrdenes from '../components/CardProOrdenes';
import { Button, Icon, Input } from "../components";

const { width } = Dimensions.get('screen');

export default class Favourites extends Component {

  constructor() {
    super()
    this.state = {
      Loading: true
    }
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({
        Loading: false
      })
    }, 2000)
  }

  render() {
    const { navigation } = this.props;
    return (
      <Block center style={styles.home}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.articles}>
          {
            this.state.Loading ?
              <Block style={{ marginTop: '80%' }}>
                <Block center style={{ justifyContent: 'center', width: 80, height: 80, backgroundColor: 'rgba(0, 0, 0, 0.8)', borderRadius: 10 }}>
                  <ActivityIndicator size='large' color={argonTheme.COLORS.WHITE} />
                </Block>
              </Block>
              :
              <Block flex>
                <CardOrdenes horizontal navigation={navigation} />
              </Block>
          }
        </ScrollView>
      </Block>
    )
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,
  },
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },

  button: {
    marginBottom: theme.SIZES.BASE,
    width: width - theme.SIZES.BASE * 2,
  },
});