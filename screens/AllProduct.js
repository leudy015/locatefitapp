import React, { Component } from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Block, theme, Text} from 'galio-framework';
import { argonTheme, tabs } from "../constants/";
import CardPro from '../components/CardPro';
import CardProVertical from '../components/CardProVertical';
const { width } = Dimensions.get('screen');


export default class AllProduct extends Component {

  renderArticles = (item) => {
    const { navigation } = this.props;
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.articles}>
        <Block flex>
          <Text
            h5
            style={{ marginBottom: theme.SIZES.BASE / 2 }}
            color={argonTheme.COLORS.DEFAULT}
          >
            Destacados.
          </Text>
          <CardPro style={{ marginRight: theme.SIZES.BASE }} navigation={navigation} />
          <Text
            h5
            style={{ marginBottom: theme.SIZES.BASE / 2 }}
            color={argonTheme.COLORS.DEFAULT}
          >
            {item.title}
          </Text>
          <Text
            style={{ marginBottom: theme.SIZES.BASE / 2 }}
            color={argonTheme.COLORS.PLACEHOLDER}
          >
            ({item.length}) resultado de la busqueda.
          </Text>
          <CardProVertical navigation={navigation} horizontal />
        </Block>
      </ScrollView>
    )
  }

  render() {
    const { navigation } = this.props;
    const data = navigation.getParam('data');
    return (
      <Block flex center style={styles.home}>
        {this.renderArticles(data)}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
});