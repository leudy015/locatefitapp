import React, { Component } from 'react';
import { StyleSheet, Image , View, Dimensions,  } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Block, theme, Text } from 'galio-framework';
import { Card, Button } from '../components';
import { Images, argonTheme } from "../constants";
const { width } = Dimensions.get('screen');





export default class Favourites extends Component {

  render() {
    const { navigation } = this.props;
    return (
      <View>
        <ScrollView showsHorizontalScrollIndicator={false}>
        <Block center style={{marginTop: 50}}>
            <Image source={Images.Astronaura}  style={{ width: 150, height: 150, marginBottom: 40}}/>
          <Block center style={{marginBottom: 30}}>
              <Text style={{color: argonTheme.COLORS.PLACEHOLDER, textAlign: 'center'}}>Aún no tienes notificacion, estas al día, bien hecho.</Text>
          </Block>
        </Block>
        
        </ScrollView>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
  button: {
    marginBottom: theme.SIZES.BASE,
    width: width - theme.SIZES.BASE * 2,
  },

});