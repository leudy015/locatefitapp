import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, ActivityIndicator } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Card } from '../components';
import { Block, theme, Text, Button } from 'galio-framework';
import { Images, argonTheme } from "../constants";
import CardProFavourite from '../components/CardProFavourites';
import ToastNotification from 'react-native-toast-notification'
const { width } = Dimensions.get('screen');


export default class Favourites extends Component {

  constructor() {
    super()
    this.state = {
      Loading: true
    }
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({
        Loading: false
      })
    }, 2000)
  }

  renderArticles = () => {
    const { navigation } = this.props;
   
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.articles}>
        {
          this.state.Loading ?
          <Block style={{ marginTop: '80%'}}>
            <Block center style={{ justifyContent: 'center', width: 80, height: 80, backgroundColor: 'rgba(0, 0, 0, 0.8)', borderRadius: 10 }}>
              <ActivityIndicator size='large' color={argonTheme.COLORS.WHITE} />
            </Block>
            </Block>
            :
            <Block flex>
              <CardProFavourite horizontal navigation={navigation} />
            </Block>
        }
      </ScrollView>
    )
  }

  render() {
    return (
      <Block flex center style={styles.home}>
        {this.renderArticles()}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
  button: {
    marginBottom: theme.SIZES.BASE,
    width: width - theme.SIZES.BASE * 2,
  },
});