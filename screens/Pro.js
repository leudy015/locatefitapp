import React from 'react';
import { ScrollView, Image, StyleSheet, StatusBar, Dimensions, Platform, View, TouchableOpacity, FlatList } from 'react-native';
import { Block, Button, Text, theme, Slider } from 'galio-framework';
import { Avatar } from 'react-native-elements';
import ViewSlider from 'react-native-view-slider';
import Coments from '../components/Coments';
import Select from '../components/Select';
import MapView from 'react-native-maps';
import Icon from '../components/Icon';
import Headerparallax from '../components/HeadersParallax';
const { height, width } = Dimensions.get('screen');
import  { NETWORK_INTERFACE_LINK } from '../constants/config';

export default class Pro extends React.Component {

  _renderItem(item) {
    const { navigation } = this.props;
    return (
    <Headerparallax name={<Text>{item.creator.nombre} {item.creator.apellidos}</Text>}
        descripcion={<Text style={{ fontSize: 14, fontWeight: '400', color: 'white' }}>
          <Icon family="Font-Awesome" name="star" size={18} color="#FFCE4E" /> 4.6 Valoración  (24) · {item.category.title} ·  <Icon family="Font-Awesome" name="map-marker" size={18} color="#95ca3e" /> {item.city}
        </Text>}
        back={<Icon family="Font-Awesome" name="times" size={20} color="#95ca3e" onPress={() => navigation.navigate('Home')}/>}
        back2={<Icon family="Font-Awesome" size={24} name="share" color="#95ca3e" />}
        avatar={{ uri: NETWORK_INTERFACE_LINK + '/assets/images/' + item.creator.foto_del_perfil}}
        imagen={
        item.fileList.length > 0 ?
             { uri: item.fileList.length > 0 ?  NETWORK_INTERFACE_LINK + "/assets/images/" + item.fileList[0] : "" }: null}
        component={
          <Block flex style={styles.container}>
            <View style={{ flexDirection: 'row', width: '100%', padding: 15, }}>
              <View style={{ width: '75%' }}>
                <Text style={{ fontSize: 20, fontWeight: '400', color: '#000', padding: 3 }}>
                  {item.title}
                </Text>
              </View>
              <View style={{ width: '25%', flexDirection: 'row' }}>
                <Text style={{ fontSize: 24, fontWeight: '700', color: '#95ca3e', marginLeft: 'auto' }}>
                  {item.number}€
            </Text>
                <Text style={{ fontSize: 16, fontWeight: '400', color: '#999', marginTop: 5, marginLeft: 'auto' }}>
                  {item.currency}
                </Text>
              </View>
            </View>
            <View style={{ width: '100%', paddingLeft: 15, paddingRight: 15, }}>
              <Text style={{ fontSize: 16, fontWeight: '200', color: '#aaa', marginTop: 5 }}>
                {item.description}
              </Text>
            </View>

            <View style={{ marginLeft: 15, marginTop: 15 }}>
              <Text style={{ padding: 5 }}>
              <Icon family="Font-Awesome" name="star" size={18} color="#FFCE4E" />
              <Icon family="Font-Awesome" name="star" size={18} color="#FFCE4E" />
              <Icon family="Font-Awesome" name="star" size={18} color="#FFCE4E" />
              <Icon family="Font-Awesome" name="star" size={18} color="#FFCE4E" />
              <Icon family="Font-Awesome" name="star" size={18} color="#FFCE4E" />
            </Text>

            </View>

            <View style={{ width: '100%', justifyContent: 'flex-start' }}>

              <View style={{ justifyContent: 'flex-start', flexDirection: 'row', marginTop: 20, marginLeft: 15 }}>
                <View style={styles.qubo}>
                  <Text style={{ fontSize: 12, fontWeight: '400', marginBottom: 5 }}>
                    <Icon family="Font-Awesome" size={18} name="history" color="#3e5c0a" />
                  </Text>
                  <Text style={{ fontSize: 12, fontWeight: '400', color: '#3e5c0a', textAlign: 'center' }}>
                    Tiempo limitado!
                  </Text>
                </View>

                <View style={styles.qubo}>
                  <Text style={{ fontSize: 12, fontWeight: '400', marginBottom: 5 }}>
                    <Icon family="Font-Awesome" size={18} name="eye" color="#3e5c0a" />
                  </Text>
                  <Text style={{ fontSize: 12, fontWeight: '400', color: '#3e5c0a', textAlign: 'center' }}>
                    +{item.visitas} visitas
                  </Text>
                </View>

                <View style={styles.qubo}>
                  <Text style={{ fontSize: 12, fontWeight: '400', marginBottom: 5 }}>
                    <Icon family="Font-Awesome" size={18} name="star" color="#3e5c0a" />
                  </Text>
                  <Text style={{ fontSize: 12, fontWeight: '400', color: '#3e5c0a', textAlign: 'center' }}>
                    (24) Opiniones
                  </Text>
                </View>

              </View>
            </View>

            <View style={styles.navigatebajo}>
              <Icon style={{marginLeft: 15, marginRight: 20}} family="Font-Awesome" name="heart" size={38} color="#ddd" />
              <TouchableOpacity
                onPress={() => navigation.navigate('Agenda')}
                style={{
                  width: "80%",
                  marginRight: 15,
                  height: 40,
                  backgroundColor: '#95ca3e',
                  borderRadius: 50,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text style={{ fontSize: 16, alignItems: "center", color: '#FFFFFF', justifyContent: "center", }}>Contratar profesional</Text>
              </TouchableOpacity>
            </View>
            <View style={{ width: '100%'}}>
              <Text style={{ fontSize: 20, marginLeft: 15, fontWeight: '300', color: '#444', }}>
                Perfil del profesional
              </Text>
              <View style={{ width: '100%', height: 85, flexDirection: 'row', marginTop: 20, color: "464646", padding: 17 }}>
                <Avatar
                  size="medium"
                  rounded
                  source={{ uri: NETWORK_INTERFACE_LINK + '/assets/images/' + item.creator.foto_del_perfil }}
                />
                <View>
                  <Text style={{ fontSize: 20, marginLeft: 15, fontWeight: '300', color: '#95ca3e', }}>
                    {item.creator.nombre} {item.creator.apellidos}
                  </Text>
                  <Text style={{ fontSize: 12, marginLeft: 15, fontWeight: '300', color: '#333', }}>
                    {item.creator.profesion}
                  </Text>
                </View>
                <Text onPress={() => navigation.navigate('Profilescreen', { data: item })} style={{ fontSize: 14, color: "#95ca3e", marginTop: 18, textAlign: 'right', alignItems: 'flex-end', marginLeft: 'auto' }}>
                  Más detalles
            </Text>
              </View>
            </View>

            <View style={{ width: '100%' }}>

              <View style={{ width: '80%', height: 100, backgroundColor: "#transparent", color: "464646", padding: 17, flexDirection: 'row' }}>
                <Text style={{ fontSize: 14, color: "#aaa" }}>
                  Para proteger tus pagos, nunca trasfiera dinero ni te comuniques fuera de la página o la aplicación de Locatefit.
                            </Text>

                <Image style={{ width: 80, height: 80, marginLeft: 10, marginTop: -15 }}
                  source={{ uri: 'https://bitbucket.org/leudy015/app-locatefit/raw/af523e73e8bf95f2902045fdd6c31d037887cd07/src/common/escudo.png' }}
                />
              </View>
            </View>
            <Text style={{ fontSize: 20, fontWeight: '300', color: '#444', marginLeft: 15, marginBottom: 20 }}>
              Ubicación
            </Text>

            <View style={{ height: 320, marginLeft: 15, marginTop: 10, marginRight: 15 }}>
              <View style={styles.container11}>
                <MapView
                  style={styles.map}
                  region={{
                    latitude: 42.3439925,
                    longitude: -3.696906,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121,
                  }}
                >
                </MapView>
              </View>
            </View>

            <View style={{ width: '100%', marginTop: 20, marginBottom: 20 }}>
              <Text style={{ fontSize: 20, marginLeft: 15, fontWeight: '300', color: '#444', marginBottom: 15 }}>
                Opiniones de clientes
            </Text>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <Coments />
                <Coments />
                <Coments />
                <Coments />
                <Coments />
                <Coments />
                <Coments />
                <Coments />
              </ScrollView>
            </View>
          </Block>

        }
      />

    );
  }

  render() {
    const { navigation } = this.props;
    const data = navigation.getParam('data');
    return this._renderItem(data);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  slider: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  dotContainer: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: -10
  },
  footer: {
    height: 100,
    width: '100%',
    backgroundColor: 'white',
    marginBottom: 0,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  navigatebajo:{
    width: '100%',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    height: 80,
    marginTop: 30,
    marginRight:  15,
  },

  qubo: {
    flexDirection: 'column',
    backgroundColor: '#CDD89B',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    padding: 10,
    width: 90,
    height: 90,
    borderRadius: 15,
    marginRight: 20,
    color: '#4B4B3A'
  },

  container11: {
    ...StyleSheet.absoluteFillObject,
    height: 300,
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },

  dotContainer: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: -5

  }

});
