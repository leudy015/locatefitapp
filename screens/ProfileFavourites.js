import React from "react";
import {
  StyleSheet,
  Dimensions,
  ScrollView,
  Image,
  ImageBackground,
  Platform,
  View
} from "react-native";
import { Block, Text, Button as GaButton, theme } from "galio-framework";
import { Button } from "../components";
import { Images, argonTheme } from "../constants";
import { SocialIcon } from 'react-native-elements'
import { HeaderHeight } from "../constants/utils";
import { NETWORK_INTERFACE_LINK } from '../constants/config';
import SocialButton from '../components/socialbutton';
const { width, height } = Dimensions.get("screen");
const thumbMeasure = (width - 48 - 32) / 3;


class Profile extends React.Component {

  _renderProfesional(item) {
    return (
      <Block flex style={styles.profile}>
        <Block flex>
          <ImageBackground
            source={Images.ProfileBackground}
            style={styles.profileContainer}
            imageStyle={styles.profileBackground}
          >
            <ScrollView
              showsVerticalScrollIndicator={false}
              style={{ width, marginTop: '25%' }}
            >
              <Block flex style={styles.profileCard}>
                <Block middle style={styles.avatarContainer}>
                  <Image
                    source={{ uri: NETWORK_INTERFACE_LINK + '/assets/images/' + item.producto.creator.foto_del_perfil }}
                    style={styles.avatar}
                  />
                </Block>
                <Block style={styles.info}>
                  <Block row center style={{ textAlign: 'center' }}>
                    <Block center>
                      <Text
                        bold
                        size={12}
                        color="#1c3643"
                        style={{ marginBottom: 4, textAlign: 'center', marginRight: 20 }}
                      >
                        247
                    </Text>
                      <Text size={12} style={{ marginRight: 20, textAlign: 'center' }}>Ordenes</Text>
                    </Block>
                    <Block center style={{ textAlign: 'center' }}>
                      <Text
                        bold
                        color="#1c3643"
                        size={12}
                        style={{ marginBottom: 4, textAlign: 'center', marginRight: 20 }}
                      >
                        89
                    </Text>
                      <Text size={12} style={{ marginRight: 20, textAlign: 'center' }}>Valoraciones</Text>
                    </Block>
                  </Block>
                </Block>
                <Block flex>
                  <Block middle style={styles.nameInfo}>
                    <Text bold size={28} color="#1c3643">
                    {item.producto.creator.nombre} {item.producto.creator.apellidos}
                    </Text>
                    <Text size={16} color="#1c3643" style={{ marginTop: 10 }}>
                    {item.producto.creator.ciudad}, España 🇪🇸
                  </Text>
                  </Block>
                  <Block middle style={{ marginTop: 30, marginBottom: 16 }}>
                    <Block style={styles.divider} />
                  </Block>
                  <Block middle>
                    <Text
                      size={16}
                      color="#1c3643"
                      style={{ textAlign: "center" }}
                    >
                      {item.producto.creator.descripcion}
                    </Text>
                  </Block>
                  <Block
                    row
                    style={{ paddingBottom: 20, justifyContent: "flex-end" }}
                  >
                  </Block>
                  <Block style={{ paddingBottom: -HeaderHeight * 2 }}>
                    <Block row space="between" style={{ flexWrap: "wrap" }}>
                      <View style={styles.input}>
                        <Text bold size={16} color="#1c3643" style={{ marginBottom: 15 }}>
                          Información profesional
                      </Text>
                        <Text size={14} color="#9FA5AA">
                          Profesión:   {item.producto.creator.profesion}
                        </Text>
                        <Text  size={14} color="#9FA5AA">
                          Estudio:  {item.producto.creator.estudios}
                        </Text >
                        <Text size={14} color="#9FA5AA" style={{ marginBottom: 15 }}>
                          Grado de experiencia:   {item.producto.creator.grado}
                        </Text>
                      </View>

                      <View style={styles.input}>
                        <Text bold size={16} color="#1c3643" style={{ marginBottom: 15 }}>
                          Información de contacto
                      </Text>
                        <Text  size={14} color="#9FA5AA">
                          Email:   {item.producto.creator.email}
                        </Text>
                        <Text  size={14} color="#9FA5AA">
                          Número movil:  {item.producto.creator.telefono}
                        </Text >
                      </View>
                    </Block>
                  </Block>
                </Block>
                <Block center style={{ width: '100%' }}>
                  <Block flex center style={styles.group}>
                    <Text bold size={16} color="#1c3643" style={{ marginBottom: 15, marginTop: 15 }}>
                      Redes sociales
                  </Text>
                    <Block style={{ width: '100%' }}>
                      <Block row space="between">
                        <SocialButton />
                      </Block>
                    </Block>
                  </Block>
                </Block>
              </Block>
            </ScrollView>
          </ImageBackground>
        </Block>
      </Block>);
  }
  render() {
    const { navigation } = this.props;
    console.log(navigation)
    const data = navigation.getParam('data');
    return this._renderProfesional(data)
    
  }
}

const styles = StyleSheet.create({
  profile: {
    marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
    // marginBottom: -HeaderHeight * 2,
    flex: 1
  },
  profileContainer: {
    width: width,
    height: height,
    padding: 0,
    zIndex: 1
  },
  profileBackground: {
    width: width,
    height: height / 2
  },
  profileCard: {
    // position: "relative",
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: 65,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2
  },
  info: {
    paddingHorizontal: 40,
    marginTop: 20
  },
  avatarContainer: {
    position: "relative",
    marginTop: -80
  },
  avatar: {
    width: 124,
    height: 124,
    borderRadius: 62,
    borderWidth: 0
  },
  nameInfo: {
    marginTop: 35
  },
  divider: {
    width: "90%",
    borderWidth: 1,
    borderColor: "#E9ECEF"
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: "center",
    width: thumbMeasure,
    height: thumbMeasure
  }
});

export default Profile;
