import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Icon, Input } from "../components";
import { Block, Checkbox, theme, Text } from "galio-framework";

export default class PaymentStrype extends Component {

  render() {
    const { navigation } = this.props;
    return (
      <View style={{ marginTop: 20, justifyContent: 'center' }}>
        <Text style={{ textAlign: 'center' }}>Stripe forms</Text>

        <Block center style={{ paddingHorizontal: theme.SIZES.BASE, marginTop: 30, width: 280 }}>
          <Button color="primary" onPress={() => navigation.navigate('Thank')}>
           COMPLETAR EL PAGO
          </Button>
        </Block>
      </View>

    );
  }
}

const styles = StyleSheet.create({


});