import React, { Component } from 'react';
import { StyleSheet, View, Image, Alert, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import { ListItem } from 'react-native-elements'
import { Button, Icon, Input } from "../components";
import { Images, argonTheme } from "../constants";
import { Block, Checkbox, theme, Text } from "galio-framework";


const { width, height } = Dimensions.get("screen");

const list = [
    {
        name: 'Ofrecido por: Leudy Martes',
        avatar_url: 'https://www.canariasnoticias.es/sites/default/files/2019/01/61587.jpg',
        subtitle: 'Servicio de Fontanería'
    },

]


export default class Registration extends Component {



    render() {
        const { navigation } = this.props;

        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ marginTop: 20 }}>
                            {
                                list.map((l, i) => (
                                    <ListItem
                                        topDivider
                                        key={i}
                                        leftAvatar={{ source: { uri: l.avatar_url } }}
                                        title={l.name}
                                        subtitle={l.subtitle}
                                        bottomDivider

                                    />
                                ))
                            }
                        </View>

                        <View style={{ marginTop: 20, borderTopWidth: 1, borderBottomWidth: 1, borderBottomColor: '#ddd', borderTopColor: '#ddd', paddingBottom: 10 }}>
                            <View style={{ flexDirection: 'column', alignItems: 'flex-start', marginRight: 10, marginLeft: 15 }}>
                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    Precio unitario
                                </Text>
                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    Cantidad
                                </Text>

                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    Descuento
                            </Text>

                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    Total
                            </Text>

                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    Total a pagar
                            </Text>
                            </View>

                            <View style={{ flexDirection: 'column', alignItems: 'flex-end', marginRight: 15, marginLeft: 15, marginTop: -130 }}>
                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    99,99€
                                </Text>

                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    1
                                </Text>
                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    0,00€
                                </Text>

                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    99,99€
                            </Text>

                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    99,99€
                            </Text>
                            </View>
                        </View>

                        <View style={{ alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#ddd', paddingBottom: 20 }}>
                            <Text style={{ fontWeight: '200', marginTop: 30 }}>
                                ¿Tienes un cupón?
                            </Text>
                            <Block width={width * 0.9} style={{ marginBottom: 15, marginTop: 15 }}>
                                <Input
                                    right
                                    placeholder="Añadir cupón"
                                    style={{
                                        borderColor: argonTheme.COLORS.INFO,
                                        borderRadius: 4,
                                        backgroundColor: "#fff"
                                    }}
                                    iconContent={<Block />}
                                />
                            </Block>
                            <Block center>
                                <Button color="default" >
                                    APLICAR CUPÓN
                                </Button>
                            </Block>
                        </View>

                        <View style={{ alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#ddd', paddingBottom: 20 }}>
                            <Text style={{ fontWeight: '200', marginTop: 30 }}>
                                Añadir descripción de la tarea
                            </Text>
                            <Block width={width * 0.9} style={{ marginBottom: 15, marginTop: 15 }}>
                                <Input
                                    right
                                    placeholder="Añadir descripción de la tarea"
                                    style={{
                                        borderColor: argonTheme.COLORS.INFO,
                                        borderRadius: 4,
                                        backgroundColor: "#fff"
                                    }}
                                    iconContent={<Block />}
                                />
                            </Block>
                        </View>
                        <View style={{ padding: 15, borderBottomWidth: 1, borderBottomColor: '#ddd', }}>
                            <Block  style={{ paddingHorizontal: theme.SIZES.BASE }}>
                                <Input right placeholder="Nombre completo" iconContent={<Block />} />
                            </Block>
                            <Block  style={{ paddingHorizontal: theme.SIZES.BASE }}>
                                <Input right placeholder="Calle" iconContent={<Block />} />
                            </Block>
                            <Block  style={{ paddingHorizontal: theme.SIZES.BASE, width: 250, flexDirection: 'row' }}>
                                <Input style={{marginRight: 30}} right placeholder="Ciudad" iconContent={<Block />} />
                                <Input style={{marginLeft: 37 }}right placeholder="Provincia" iconContent={<Block />} />
                            </Block>

                            <Block  style={{ paddingHorizontal: theme.SIZES.BASE, width: 250, flexDirection: 'row' }}>
                                <Input style={{marginRight: 30}} right placeholder="Codígo postal" iconContent={<Block />} />
                                <Input style={{marginLeft: 37}}right placeholder="Número movíl" iconContent={<Block />} />
                            </Block>

                            <Block center style={{ paddingHorizontal: theme.SIZES.BASE, marginTop: 30, width: 280}}>
                                <Button color="default" >
                                    GUARDAR CAMBIOS
                                </Button>
                            </Block>
                           
                        </View>
                        <View style={{ marginTop: 20, borderTopWidth: 1, borderBottomWidth: 1, borderBottomColor: '#ddd', borderTopColor: '#ddd', paddingBottom: 10 }}>
                        <Block row width={width * 0.9} style={{marginLeft: 15, marginTop: 15}}>
                            <Checkbox
                                checkboxStyle={{
                                borderWidth: 3
                                }}
                                color={argonTheme.COLORS.PRIMARY}
                                label="Acepto las"
                            />
                            <Button
                                style={{ width: 250 }}
                                color="transparent"
                                textStyle={{
                                color: argonTheme.COLORS.PRIMARY,
                                fontSize: 14
                                }}
                            >
                        Condiciones de uso y compra
                      </Button>
                    </Block>
                        </View>
                        <View style={{ alignItems: 'center', marginTop: 20, marginBottom: 30 }}>

                            <Block center style={{ paddingHorizontal: theme.SIZES.BASE, marginTop: 30, width: 280}}>
                                <Button color="primary" onPress={() => navigation.navigate('PaymentStripe')}>
                                    PAGAR CON TARJETA
                                </Button>
                            </Block>

                            <Block center style={{ paddingHorizontal: theme.SIZES.BASE, marginTop: 30, width: 280}}>
                                <Button color="primary" onPress={() => navigation.navigate('PaymentPaypal')}>
                                    PAGAR CON PAYPAL
                                </Button>
                            </Block>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    icons: {
        marginTop: 20

    },

    cardtarjeta: {
        color: "#DDD",
        width: 300,
        height: 60,
        backgroundColor: "#f8f9fa",
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        paddingLeft: 30,
        paddingRight: 30,

    },

    header: {
        width: "100%",
        height: '15%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',

    },
    body: {
        width: "100%",
        height: '100%',
    },
    Logo: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textView: {
        width: "100%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",


    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        marginBottom: 10,
        width: '95%',
        flexDirection: 'row'
    }
});