import React, { Component } from 'react';
import { StyleSheet, Image, Dimensions, ActivityIndicator } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Block, theme, Text } from 'galio-framework';
import { Card, Button } from '../components';
import CardPedidos from '../components/CardProPedidos';
import { Images, argonTheme } from "../constants";
const { width } = Dimensions.get('screen');




export default class Favourites extends Component {

  constructor() {
    super()
    this.state = {
      Loading: true
    }
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({
        Loading: false
      })
    }, 2000)
  }

  render() {
    const { navigation } = this.props;
    return (
      <Block center style={styles.home}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.articles}>

          {
            this.state.Loading ?
              <Block style={{ marginTop: '80%' }}>
                <Block center style={{ justifyContent: 'center', width: 80, height: 80, backgroundColor: 'rgba(0, 0, 0, 0.8)', borderRadius: 10 }}>
                  <ActivityIndicator size='large' color={argonTheme.COLORS.WHITE} />
                </Block>
              </Block>
              :
              <Block flex>
                <CardPedidos horizontal navigation={navigation} />
              </Block>

          }
        </ScrollView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
  optionsButton: {
    width: "auto",
    height: 34,
    paddingHorizontal: theme.SIZES.BASE,
    paddingVertical: 10,
    marginTop: 20,
    marginLeft: 'auto',
  },
});