import React, { Component } from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Block, theme, Text } from 'galio-framework';
import { Button, Input } from '../components';
import { argonTheme } from "../constants";
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
const { width } = Dimensions.get('screen');

const USUARIO_PAGO_QUERY = gql`
    query{
      getUsuarioPago {
        success
        message
        data {
          id
          nombre
          iban
          }
        }
      }
    `;

const ELIMINAR_PAGO = gql`
  mutation eliminarPago($id: ID!) {
    eliminarPago(id: $id) {
      success
      message
    }
  }
`;

const NUEVO_PAGO = gql`
  mutation crearPago($input: PagoInput) {
    crearPago(input: $input) {
      success
      message
      data {
        id
        nombre
        iban
      }
    }
  }
`;



export default class OrderDetail extends Component {

  constructor(props) {
    super(props);
    this.state = {
      iban: '',
      nombre: '',
      fetched: false,
    };
  }
 

  actualizarState = (name, value) => {
    this.setState({
      [name]: value
    });
    console.log(value)
  };

  handleSubmit(e, crearPago) {
    e.preventDefault();
        const input = this.state;
        console.log('sending input', input)
        crearPago({ variables: { input } }).then(async ({ data: res }) => {
          if (res && res.crearPago && res.crearPago.success) {
            console.log(res.crearPago.message);
          }
          else if (res && res.crearPago && !res.crearPago.success)
           console.log(res.crearPago.message);
        });
      };

      handleEliminar(e, eliminarPago, id) {
        e.preventDefault();
        eliminarPago({ variables: { id } }).then(async ({ data: res }) => {
          if (res && res.eliminarPago && res.eliminarPago.success) {
            console.log(res.eliminarPago.message);
          }
          else if (res && res.eliminarPago && !res.eliminarPago.success)
            console.log(res.eliminarPago.message);
        });
      }

  render() {
    const { iban, nombre, } = this.state;
    return (
      <Block>
        <ScrollView showsHorizontalScrollIndicator={false}>
          <Block style={{ marginBottom: 5, marginTop: 5 }}>
            <Text bold size={18} color="#1c3643" style={{ marginTop: 15, marginLeft: 15 }}>
              Mis datos de cobro
          </Text>
            <Text size={14} color="#1c3643" style={{ marginTop: 15, marginLeft: 15, }}>
              Aqui podrás editar tu información para retirar cobros de la plataforma
          </Text>
          </Block>

          <Query query={USUARIO_PAGO_QUERY}>
            {(response, error, refetch) => {
              this.refetch = refetch;
              if (error) {
                console.log('Response Error-------', error);
                return <Text style={styles.errorText}>{error}</Text>
              }
              if (response) {
                return (
                  <Block center style={styles.payment}>
                    <Text bold size={20} color="#1c3643" style={{ marginTop: 15, marginLeft: 15 }}>
                      {response && response.data && response.data.getUsuarioPago.data ? response.data.getUsuarioPago.data.nombre : ''}
                    </Text>
                    <Text size={14} color="#1c3643" style={{ marginTop: 15, marginLeft: 15, }}>
                      {response && response.data && response.data.getUsuarioPago.data ? response.data.getUsuarioPago.data.iban : ''}
                    </Text>
                    <Mutation mutation={ELIMINAR_PAGO}>
                      {(eliminarPago, { loading, error, data }) => {
                        this.refetch = refetch;
                        return (
                          <Block center style={{ marginTop: 30 }}>
                            <Button color="error" style={{ width: 200 }} onPress={e => this.handleEliminar(e, eliminarPago, response.data.getUsuarioPago.data.id, refetch)}>
                              ELIMINAR
                          </Button>
                          </Block>
                        )
                      }}
                    </Mutation>
                  </Block>
                )
              }
            }}
          </Query>
          <Mutation mutation={NUEVO_PAGO}>
            {(crearPago, {refetch}) => {
              return (
                <Block>
                <Block flex center width={width * 0.9} style={{ marginBottom: 5, marginTop: 25 }}>
                  <Input
                    right
                    placeholder="Nombre completo"
                    name='nombre'
                    value={nombre}
                    onChangeText={(text) => this.actualizarState('nombre', text)}
                    style={{
                      borderColor: argonTheme.COLORS.DEFAULT,
                      borderRadius: 4,
                      backgroundColor: "#fff"
                    }}
                    iconContent={<Block />}
                  />
                </Block>

                <Block flex center width={width * 0.9} style={{ marginBottom: 5, marginTop: 5 }}>
                  <Input
                    right
                    name='iban'
                    value={iban}
                    onChangeText={(text) => this.actualizarState('iban', text)}
                    placeholder="IBAN Bancarío"
                    style={{
                      borderColor: argonTheme.COLORS.DEFAULT,
                      borderRadius: 4,
                      backgroundColor: "#fff"
                    }}
                    iconContent={<Block />}
                  />
                </Block>

                <Block center style={{ paddingHorizontal: theme.SIZES.BASE, marginTop: 30 }}>
                  <Button color="default" onPress={e => this.handleSubmit(e, crearPago, refetch)}>
                    GUARDAR CAMBIOS
                </Button>
                </Block>
                </Block>
                )
              }}
            </Mutation>
        </ScrollView>
      </Block>
        );
      }
    }
    
const styles = StyleSheet.create({
          articles: {
          width: width - theme.SIZES.BASE * 2,
        paddingVertical: theme.SIZES.BASE,
      },
  optionsButton: {
          width: "auto",
        height: 34,
        paddingHorizontal: theme.SIZES.BASE,
        paddingVertical: 10,
        marginTop: 10,
        marginLeft: 'auto',
        marginRight: 15
      },
  payment: {
          height: 'auto',
        width: '90%',
        backgroundColor: argonTheme.COLORS.WHITE,
        marginTop: 20,
        borderRadius: 10,
        paddingVertical: theme.SIZES.BASE * 2,
        shadowColor: 'black',
    shadowOffset: {width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.2,
        elevation: 3,
      }
});