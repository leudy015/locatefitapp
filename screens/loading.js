import React, { Component } from "react";
import { ActivityIndicator} from 'react-native';
import { Block , theme} from 'galio-framework';
import { argonTheme } from "../constants/";
import Homecomponet from './Homecomponent';

class Loadingstate extends Component {

    constructor() {
        super()
        this.state = {
            Loading: true
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                Loading: false
            })
        }, 3000)
    }

    render() {
        return (
            <Block>
                {
                this.state.Loading ?
                    <Block center style={{marginTop: '60%' }}>
                    <Block style={{ justifyContent: 'center', width: 80, height: 80, backgroundColor: 'rgba(0, 0, 0, 0.8)', borderRadius: 10}}>
                        <ActivityIndicator size='large' color={argonTheme.COLORS.WHITE} />
                    </Block> 
                    </Block> 
                    :
                    <Block>
                        <Homecomponet />
                    </Block>
                }
            </Block>

        )
    }
}

export default Loadingstate;