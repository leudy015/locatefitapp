import React from "react";
import {
  StyleSheet,
  ImageBackground,
  Dimensions,
  StatusBar,
  KeyboardAvoidingView,
  AsyncStorage,
  Alert
} from "react-native";
import { Block, Checkbox, Text, theme } from "galio-framework";
import { Button, Icon, Input } from "../components";
import { Images, argonTheme } from "../constants";
import { Mutation } from 'react-apollo';
import SocialLogin from '../components/sociallogin';
import gql from 'graphql-tag';
const { width, height } = Dimensions.get("screen");


const AUTENTICA_USUARIO = gql`
mutation autenticarUsuario($usuario: String!, $password: String!) {
  autenticarUsuario(usuario: $usuario, password: $password) {
    success
    message
    data {
      token
      id
    }
  }
}
`

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = { 
      usuario: '',
      password: ''
    };
  }

  actualizarState = (name, value) => {
    this.setState({
      [name]: value
    });
  };

  onCompletedLogin = async (res) => {
    const {
      usuario,
      password,
    } = this.state

    if (password=='', usuario==''){
      Alert.alert(
        'Error con el inicio de sesión',
        'Todos los campos son obligatorio para el inicio de sesión',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
        )
        return null
    }  
    console.log('res: ', res)
    if(!res.autenticarUsuario.success) {
      
      Alert.alert(
        'Error con el Inicio de sesión',
        (res.autenticarUsuario.message),
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
        ),
        this.actualizarState('usuario',  "")
        this.actualizarState('password',  "")
        return ;
      }
        await AsyncStorage.setItem('token', res.autenticarUsuario.data.token);
        await AsyncStorage.setItem('id', res.autenticarUsuario.data.id);
        this.actualizarState('usuario',  "")
        this.actualizarState('password',  "")
        this.props.navigation.navigate('Home');
      }  
  
  render() {
    const { usuario, password } = this.state
    const { navigation } = this.props;
    
    return (
      <Mutation mutation={AUTENTICA_USUARIO} variables={{ usuario, password }} onCompleted={this.onCompletedLogin}>
        {(mutation) => {
          
          return (
            <Block flex middle>
              <StatusBar hidden />
              <ImageBackground
                source={Images.RegisterBackground}
                style={{ width, height, zIndex: 1 }}
              >
                <Block flex middle>
                  <Block style={styles.registerContainer}>
                    <Block flex={0.25} middle style={styles.socialConnect}>
                      <Text color="#8898AA" size={12}>
                        Inicia sesión con
                      </Text>
                      <SocialLogin />
                    </Block>
                    <Block flex>
                      <Block flex={0.17} middle>
                        <Text color="#8898AA" size={12}>
                          O inicia con tu correo electrónico y contraseña
                  </Text>
                      </Block>
                      <Block flex center>
                        <KeyboardAvoidingView
                          style={{ flex: 1 }}
                          behavior="padding"
                          enabled
                        >

                          <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                            <Input
                              borderless
                              name='usuario'
                              onChangeText={value => this.actualizarState('usuario', value)}
                              value={usuario}
                              placeholder="Nombre de usuario"
                              iconContent={
                                <Icon
                                  size={16}
                                  color={argonTheme.COLORS.ICON}
                                  name="user"
                                  family="Font-Awesome"
                                  style={styles.inputIcons}
                                />
                              }
                            />
                          </Block>
                          <Block width={width * 0.8}>
                            <Input
                              onChangeText={value => this.actualizarState('password', value)}
                              value={password}
                              name='password'
                              password
                              borderless
                              placeholder="Contraseña"
                              iconContent={
                                <Icon
                                  size={16}
                                  color={argonTheme.COLORS.ICON}
                                  name="padlock-unlocked"
                                  family="ArgonExtra"
                                  style={styles.inputIcons}
                                />
                              }
                            />
                          </Block>
                          <Block middle>
                            <Button onPress={mutation} color="primary" style={styles.createButton}>
                              <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                                INICIAR SESIÓN
                            </Text>
                            </Button>
                          </Block>
                        <Block center row width={width * 0.75}>
                          <Text bold size={14} color={argonTheme.COLORS.BLACK}>
                            ¿Has olvidado tu contraseña?
                      </Text>
                          <Button
                            onPress={() => navigation.navigate("Forgot")}
                            style={{ width: 150 }}
                            color="transparent"
                            textStyle={{
                              color: argonTheme.COLORS.PRIMARY,
                              fontSize: 14
                            }}
                          >
                            RECUPERAR
                      </Button>
                        </Block>
                        </KeyboardAvoidingView>
                    </Block>
                  </Block>
                </Block>
                </Block>
              </ImageBackground>
            </Block>
    );
  }
}
      </Mutation >
    );
  }
}

const styles = StyleSheet.create({
  registerContainer: {
    width: width * 0.9,
    height: height * 0.78,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },
  socialConnect: {
    backgroundColor: argonTheme.COLORS.WHITE,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: "#8898AA",
  },
  socialButtons: {
    width: 120,
    height: 40,
    backgroundColor: "#fff",
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1
  },
  socialTextButtons: {
    color: argonTheme.COLORS.PRIMARY,
    fontWeight: "800",
    fontSize: 14
  },
  inputIcons: {
    marginRight: 12
  },
  passwordCheck: {
    paddingLeft: 15,
    paddingTop: 13,
    paddingBottom: 30
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25,
    marginBottom: 25
  }
});

export default Login;
