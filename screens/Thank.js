import React, { Component } from 'react';
import { StyleSheet, View, Image} from 'react-native';
import { Button, Icon, Input } from "../components";
import { Block, Checkbox, theme, Text } from "galio-framework";
import Check from "../assets/imgs/check.png"

export default class PaymentStrype extends Component {

  render() {
    const { navigation } = this.props;
    return (
      <View style={{ marginTop: 20, justifyContent: 'center' }}>
        <Text style={{ textAlign: 'center' }}>¡Profesional contratado con éxito en unos minutos te estará tocando el telefonillo!</Text>
        <Block center style={{marginTop: 30, marginButton: 30 }}>
          <Image source={Check} />
          </Block>
        <Block center style={{ paddingHorizontal: theme.SIZES.BASE, marginTop: 30, width: 280 }}>
          <Button color="primary" onPress={() => navigation.navigate('Pedidoscreen')}>
           Ir a mis pedido
          </Button>
        </Block>
      </View>

    );
  }
}