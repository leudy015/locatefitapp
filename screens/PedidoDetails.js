import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Block, theme, Text } from 'galio-framework';
import { Card, Button } from '../components';
import Icon from '../components/Icon';
import { argonTheme } from "../constants/";
import articles from '../constants/articles';
import { Rating, Avatar } from 'react-native-elements';
import OpenURLButton from '../components/Linkout';
const { width } = Dimensions.get('screen');


export default class PedidoDetail extends Component {

  _renderItem(item) {
      const { navigation } = this.props;
      const description = item.nota;
      const titulo = item.product.title;
      const date = new Date(item.endDate + ' ' + item.time);
      const year = date.getFullYear();
      // const year = date.getUTCFullYear();
      let month = date.getMonth() + 1;
      // let month = date.getUTCMonth() + 1;
      month = (month < 10) ? '0' + month : month;
      const day = date.getDate();
      // const day = date.getUTCDate();
      let hours = date.getHours();
      // let hours = date.getUTCHours();
      hours = (hours < 10) ? '0' + hours : hours;
      const minutes = date.getMinutes();
      // const minutes = date.getUTCMinutes();

      const startDateString = year + '' + month + '' + day + 'T' + hours + '' + minutes + '000';

      const productoTime = item.product.time.split(':');
      let endHours = Number(hours) + (Number(productoTime[0]) * Number(item.cantidad));
      let endMinutes = Number(minutes) + (Number(productoTime[1]) * Number(item.cantidad));
      if (endMinutes > 60) {
        endHours++;
        endMinutes -= 60;
      }

      endHours = (endHours < 10) ? '0' + endHours : endHours;
      endMinutes = (endMinutes < 10) ? '0' + endMinutes : endMinutes;
      const endDateString = year + '' + month + '' + day + 'T' + endHours + '' + endMinutes + '00';

      link = "https://calendar.google.com/calendar/r/eventedit?text=" + titulo + "&dates=" + startDateString +  "/" + endDateString + "&details=" + description + "&sprop=name:Locatefit&sf=true";

    return (
      <Block>
        <ScrollView showsHorizontalScrollIndicator={false} style={styles.container}>
          <Block flex >
            {item.product.fileList.length > 0 ?
              <Image source={{ uri: item.product.fileList.length > 0 ? "https://server.locatefit.es/assets/images/" + item.product.fileList[0] : "" }} style={{ width: '100%', height: 300 }} /> : null}
            <Icon family="Font-Awesome" name="times" style={{ position: 'absolute', marginTop: 40, marginLeft: 20 }} size={20} color="#95ca3e" onPress={() => navigation.navigate('Pedido')} />
          </Block>

          <Block flex style={styles.cards}>
            <Text
              h5
              style={{ marginBottom: theme.SIZES.BASE / 2, marginTop: 15, marginLeft: 15 }}
              color={argonTheme.COLORS.DEFAULT}>
              {item.product.title}
            </Text>
            <Block row>
              <Text
                style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 15 }}
                color={argonTheme.COLORS.PLACEHOLDER}>
                {item.cantidad + item.product.currency}
              </Text>
              <Text size={12} style={styles.estados}>{item.estado}</Text>
            </Block>
          </Block>
          <Block flex style={styles.cards}>
            <Text
              p
              style={{ marginBottom: theme.SIZES.BASE / 2, marginTop: 15, marginLeft: 15, marginBottom: 20 }}
              color={argonTheme.COLORS.MUTED}>
              Fecha del realización del servicio:
            </Text>

            <Text
              style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 15 }}
              color={argonTheme.COLORS.PLACEHOLDER}>
              {item.endDate} a las {item.time}
            </Text>

            <OpenURLButton color='primary' texto='AÑADIR A GOOGLE CALENDAR' url={link} />

          </Block>
          <Block flex style={styles.cards}>
            <Text
              p
              style={{ marginBottom: theme.SIZES.BASE / 2, marginTop: 15, marginLeft: 15, marginBottom: 20 }}
              color={argonTheme.COLORS.MUTED}>
              Informacón del profesional:
            </Text>

            <Block row>

              <Avatar
                containerStyle={{ marginLeft: 15 }}
                rounded
                source={{ uri: "https://server.locatefit.es/assets/images/" + item.prfsnl.foto_del_perfil }}
              />
              <Text
                p
                style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 15 }}
                color={argonTheme.COLORS.DEFAULT}>
                {item.prfsnl.nombre} {item.prfsnl.apellidos}
              </Text>

            </Block>
            <Text
              style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 15, marginTop: 20 }}
              color={argonTheme.COLORS.PLACEHOLDER}>
              Profesión: {item.prfsnl.profesion}
            </Text>
            <Text
              style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 15 }}
              color={argonTheme.COLORS.PLACEHOLDER}>
              Número móvil: {item.prfsnl.telefono}
            </Text>
            <Text
              style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 15 }}
              color={argonTheme.COLORS.PLACEHOLDER}>
              Ciudad: {item.prfsnl.ciudad}
            </Text>
            <Block left  row style={{ marginBottom: 7, marginLeft: 15}}>
             <Rating
                imageSize={20}
                readonly
                startingValue={5}
                style={{ marginLeft: 0, marginRight: 15 }}
              />
              <Block>
                <Text style={{ marginBottom: theme.SIZES.BASE / 2 }}
              color={argonTheme.COLORS.PLACEHOLDER}>(2763) Valoraciones</Text>
              </Block>
            </Block>
          </Block>

          <Block flex style={styles.cards}>
            <Text
              p
              style={{ marginBottom: theme.SIZES.BASE / 2, marginTop: 15, marginLeft: 15, marginBottom: 20 }}
              color={argonTheme.COLORS.MUTED}>
              Detalles del Pedido:
            </Text>
            <Block row style={styles.detail}>
              <Text
                style={{ marginBottom: theme.SIZES.BASE / 2 }}
                color={argonTheme.COLORS.DEFAULT}>
                Número de Pedido:
              </Text>
              <Text
                style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 'auto' }}
                color={argonTheme.COLORS.MUTED}>
                {item.id}
              </Text>
            </Block>
            <Block row style={styles.detail}>
              <Text
                style={{ marginBottom: theme.SIZES.BASE / 2 }}
                color={argonTheme.COLORS.DEFAULT}>
                Fecha:
          </Text>
              <Text
                style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 'auto' }}
                color={argonTheme.COLORS.MUTED}>
                {item.created_at}
              </Text>
            </Block>
            <Block row style={styles.detail}>
              <Text
                style={{ marginBottom: theme.SIZES.BASE / 2 }}
                color={argonTheme.COLORS.DEFAULT}>
                Total:
          </Text>
              <Text
                style={{ marginBottom: theme.SIZES.BASE / 2, marginLeft: 'auto', }}
                color={argonTheme.COLORS.MUTED}>
                {item.cantidad * item.product.number}€
          </Text>
            </Block>

            <Block style={styles.footer}>
              <OpenURLButton color='error' texto='SOLICITAR DEVOLUCIÓN' url={'mailto:locatefit.es@gmail.com'} />
            </Block>
          </Block>
        </ScrollView>

      </Block>
    )
  }

  render() {
    const { navigation } = this.props;
    console.log(navigation)
    const data = navigation.getParam('data');
    return this._renderItem(data);
  }
}

const styles = StyleSheet.create({
  container:{
      marginBottom: 20
  },
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
  optionsButton: {
    width: "auto",
    height: 34,
    paddingHorizontal: theme.SIZES.BASE,
    paddingVertical: 10,
    marginTop: 20,
    marginLeft: 'auto',
  },

  footer: {
    marginTop: 30,

  },
  button: {
    marginBottom: theme.SIZES.BASE,
    width: width - theme.SIZES.BASE * 2
  },
  estados: {
    marginLeft: 'auto',
    padding: 5,
    backgroundColor: argonTheme.COLORS.INPUT_SUCCESS,
    color: argonTheme.COLORS.WHITE,
    borderRadius: 15,
    marginRight: 15,
    marginBottom: 10
  },

  cards: {
    borderBottomColor: argonTheme.COLORS.BORDER_COLOR,
    borderBottomWidth: 0.5,


  },
  detail: {
    borderBottomColor: argonTheme.COLORS.BORDER_COLOR,
    borderBottomWidth: 0.5,
    padding: 15
  },

});