import React from "react";
import {
  StyleSheet,
  ImageBackground,
  Dimensions,
  StatusBar,
  KeyboardAvoidingView,
  Alert
} from "react-native";
import { Block, Checkbox, Text, theme } from "galio-framework";
import { Button, Icon, Input } from "../components";
import { Images, argonTheme } from "../constants";
import Sociallogin from '../components/sociallogin';
import { Mutation } from 'react-apollo';

import gql from 'graphql-tag';
import { ScrollView } from "react-native-gesture-handler";
const { width, height } = Dimensions.get("screen");

const NUEVO_USUARIO = gql`
  mutation crearUsuario($input: UsuarioInput) {
    crearUsuario(input: $input) {
      success
      message
      data {
        usuario
        password
        email
        nombre
        apellidos
      }
    }
  }
`

class Register extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      usuario: '',
      password: '',
      email: '',
      nombre: '',
      apellidos: ''
    };
  }


  actualizarState = (name, value) => {
    this.setState({
      [name]: value
    });
    console.log(value)
  };


  handleSubmit(e, crearUsuario, value) {
    e.preventDefault();
    const input = this.state
    console.log('input in handleSubmit: ', input);
    const {
      usuario,
      password,
      email,
      nombre,
      apellidos
    } = this.state
    if (password=='', usuario=='', email=='', nombre=='', apellidos==''){
      Alert.alert(
        'Error con el registro',
        'Todos los campos son obligatorio para el registro de usuario',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
        )
        return null
    }  
    crearUsuario({ variables: { input } }).then(async ({ data: re }) => {
      if (re && re.crearUsuario && re.crearUsuario.success) {
        console.log(re.crearUsuario.message);
        this.props.navigation.navigate('ConfirmEmail');
        this.actualizarState('usuario',  "")
        this.actualizarState('password',  "")
        this.actualizarState('email',  "")
        this.actualizarState('nombre',  "")
        this.actualizarState('apellidos',  "")
      } else if (re && re.crearUsuario && !re.crearUsuario.success)
        console.log(re.crearUsuario.message);
        Alert.alert(
          'Registro de usuario',
          (re.crearUsuario.message),
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
          { cancelable: false },
          ),
        this.actualizarState('usuario',  "")
        this.actualizarState('password',  "")
        this.actualizarState('email',  "")
        this.actualizarState('nombre',  "")
        this.actualizarState('apellidos',  "")
    });
  }


  render() {
    const { usuario, password, email, nombre, apellidos} = this.state
    const { navigation } = this.props;
    return (
      <Mutation mutation={NUEVO_USUARIO}>
        {(crearUsuario) => {
          return (
            <Block flex middle>
              <StatusBar hidden />
              <ImageBackground
                source={Images.RegisterBackground}
                style={{ width, height, zIndex: 1 }}
              >
                <Block flex middle>
                  <Block style={styles.registerContainer}>
                    <Block flex={0.25} middle style={styles.socialConnect}>
                      <Text color="#8898AA" size={12}>
                        Registrarte con
                    </Text>
                      <Sociallogin />
                    </Block>
                    <Block flex>
                      <Block flex={0.17} middle>
                        <Text color="#8898AA" size={12}>
                          O regístrate con tu correo electrónico
                     </Text>
                      </Block>
                      <Block flex center>
                        <ScrollView showsHorizontalScrollIndicator={false} style={{ marginBottom: 40 }}>
                          <KeyboardAvoidingView
                            style={{ flex: 1 }}
                            behavior="padding"
                            enabled
                          >

                            <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                              <Input
                                name='nombre'
                                value={nombre}
                                onChangeText={(text) => this.actualizarState('nombre', text)}
                                borderless
                                placeholder="Nombre"
                                iconContent={
                                  <Icon
                                    size={16}
                                    color={argonTheme.COLORS.ICON}
                                    name="user"
                                    family="Font-Awesome"
                                    style={styles.inputIcons}
                                  />
                                }
                              />
                            </Block>

                            <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                              <Input
                                name='apellidos'
                                value={apellidos}
                                onChangeText={(text) => this.actualizarState('apellidos', text)}
                                borderless
                                placeholder="Apellidos"
                                iconContent={
                                  <Icon
                                    size={16}
                                    color={argonTheme.COLORS.ICON}
                                    name="user"
                                    family="Font-Awesome"
                                    style={styles.inputIcons}
                                  />
                                }
                              />
                            </Block>
                            <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                              <Input
                                name='usuario'
                                value={usuario}
                                onChangeText={(text) => this.actualizarState('usuario', text)}
                                borderless
                                placeholder="Nombre de usuario"
                                iconContent={
                                  <Icon
                                    size={16}
                                    color={argonTheme.COLORS.ICON}
                                    name="user"
                                    family="Font-Awesome"
                                    style={styles.inputIcons}
                                  />
                                }
                              />
                            </Block>
                            <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                              <Input
                                name='email'
                                value={email}
                                onChangeText={(text) => this.actualizarState('email', text)}
                                borderless
                                placeholder="Correo electrónico"
                                iconContent={
                                  <Icon
                                    size={16}
                                    color={argonTheme.COLORS.ICON}
                                    name="ic_mail_24px"
                                    family="ArgonExtra"
                                    style={styles.inputIcons}
                                  />
                                }
                              />
                            </Block>
                            <Block width={width * 0.8}>
                              <Input
                                name='password'
                                value={password}
                                onChangeText={(text) => this.actualizarState('password', text)}
                                password
                                borderless
                                placeholder="Contraseña"
                                iconContent={
                                  <Icon
                                    size={16}
                                    color={argonTheme.COLORS.ICON}
                                    name="padlock-unlocked"
                                    family="ArgonExtra"
                                    style={styles.inputIcons}
                                  />
                                }
                              />

                              <Block row style={styles.passwordCheck}>
                                <Text size={12} color={argonTheme.COLORS.MUTED}>
                                  La contraseña debe tener 8 caracteres como minímo (Números y Letras)
                        </Text>
                              </Block>
                            </Block>
                            <Block row width={width * 0.75}>
                              <Checkbox
                                checkboxStyle={{
                                  borderWidth: 3
                                }}
                                color={argonTheme.COLORS.PRIMARY}
                                label="Acepto las"
                              />
                              <Button
                                style={{ width: 150 }}
                                color="transparent"
                                textStyle={{
                                  color: argonTheme.COLORS.PRIMARY,
                                  fontSize: 14
                                }}
                              >
                                Politica de privacidad
                            </Button>
                            </Block>
                            <Block middle>
                              <Button onPress={e => this.handleSubmit(e, crearUsuario)} color="primary" style={styles.createButton}>
                                <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                                  CREAR CUENTA
                                </Text>
                              </Button>
                            </Block>
                            <Block center row width={width * 0.75}>
                              <Text bold size={14} color={argonTheme.COLORS.BLACK}>
                                ¿Ya tiene una cuenta?
                                </Text>
                              <Button
                                onPress={() => navigation.navigate("Login")}
                                style={{ width: 150 }}
                                color="transparent"
                                textStyle={{
                                  color: argonTheme.COLORS.PRIMARY,
                                  fontSize: 14
                                }}
                              >
                                INICIAR SESIÓN
                      </Button>
                            </Block>
                          </KeyboardAvoidingView>
                        </ScrollView>
                      </Block>
                    </Block>
                  </Block>
                </Block>
              </ImageBackground>
            </Block>
          );
        }}
      </Mutation>
    );
  }
}

const styles = StyleSheet.create({
  registerContainer: {
    width: width * 0.9,
    height: height * 0.78,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },
  socialConnect: {
    backgroundColor: argonTheme.COLORS.WHITE,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: "#8898AA"
  },
  socialButtons: {
    width: 120,
    height: 40,
    backgroundColor: "#fff",
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1
  },
  socialTextButtons: {
    color: argonTheme.COLORS.PRIMARY,
    fontWeight: "800",
    fontSize: 14
  },
  inputIcons: {
    marginRight: 12
  },
  passwordCheck: {
    paddingLeft: 15,
    paddingTop: 13,
    paddingBottom: 30
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25,
    marginBottom: 25
  }
});

export default Register;
