import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, AsyncStorage, Alert, ActivityIndicator } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Block, theme, Text } from 'galio-framework';
import { Button, Input, Switch } from '../components';
import { Images, argonTheme } from "../constants";
import { ApolloConsumer, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import OpenURLButton from '../components/Linkout';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../constants/config';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import { Avatar } from 'react-native-elements';

const { width } = Dimensions.get('screen');

const USER_DETAIL = gql`
  query getUsuario($id: ID!) {
    getUsuario(id: $id) {
      email
      nombre
      apellidos
      ciudad
      telefono
      foto_del_perfil
      fotos_tu_dni
      profesion
      descripcion
      fecha_de_nacimiento
      notificacion
      grado
      estudios
      formularios_de_impuesto
      fb_enlazar
      twitter_enlazar
      instagram_enlazar
      youtube_enlazar
      propia_web_enlazar
    }
  }
`;

const ACTUALIZAR_USUARIO = gql`
  mutation actualizarUsuario($input: ActualizarUsuarioInput) {
    actualizarUsuario(input: $input) {
      id
      email
      nombre
      apellidos
      ciudad
      telefono
      foto_del_perfil
      fotos_tu_dni
      profesion
      descripcion
      fecha_de_nacimiento
      notificacion
      grado
      estudios
      formularios_de_impuesto
      fb_enlazar
      twitter_enlazar
      instagram_enlazar
      youtube_enlazar
      propia_web_enlazar
    }
  }
`;

const UPLOAD_FILE = gql`
  mutation singleUpload($imgBlob: Upload) {
    singleUpload(file: $imgBlob) {
      filename
    }
  }
`;


export default class OrderDetail extends Component {

  state = {
    image: null,
    email: '',
    nombre: '',
    apellidos: '',
    profesion: '',
    telefono: '',
    foto_del_perfil: '',
    descripcion: '',
    notificacion: '',
    userId: '',
    fetched: false,
    Loading: false
  };

  _handleSubmit = async (mutation) => {
    const id = await AsyncStorage.getItem('id');
    const input = this.state;
    delete input.fetched;
    input.id = id; // cbeck now ok
    console.log(input)
    mutation({ variables: { input } }).then(res => {
      console.log('done')
    }).catch(err => console.log(err))//
  }

  componentDidMount() {
    this.getPermissionAsync();
    console.log('hi');
  }


  async componentDidMount() {
    const permission = await Permissions.getAsync(Permissions.CAMERA_ROLL);
    if (permission.status !== 'granted') {
      const newPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (newPermission.status === 'granted') {
        Alert.alert('Lo sentimos, ¡necesitamos permisos de cámara para hacer que esto funcione!');
      }
    }
  }

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
  };


  async componentDidMount() {
    const userId = await AsyncStorage.getItem('id');
    this.setState({ userId })
  }

  _handleInputValue = (name, value) => {
    this.setState({ [name]: value })
  }

  setUserData = (user) => {
    const { email,
      nombre,
      apellidos,
      ciudad,
      telefono,
      foto_del_perfil,
      fotos_tu_dni,
      profesion,
      descripcion,
      fecha_de_nacimiento,
      notificacion,
      grado,
      estudios,
      formularios_de_impuesto,
      fb_enlazar,
      twitter_enlazar,
      instagram_enlazar,
      youtube_enlazar,
      propia_web_enlazar, } = user;
    this.setState({
      email,
      nombre,
      apellidos,
      ciudad,
      telefono,
      foto_del_perfil,
      fotos_tu_dni,
      profesion,
      descripcion,
      fecha_de_nacimiento,
      notificacion,
      grado,
      estudios,
      formularios_de_impuesto,
      fb_enlazar,
      twitter_enlazar,
      instagram_enlazar,
      youtube_enlazar,
      propia_web_enlazar, fetched: true
    }) // what happend ? no
  }

  onCompleted = () => {
    console.log('working/==================');// hck now
  }

  render() {
    const {
      userId,
      email,
      nombre,
      apellidos,
      telefono,
      profesion,
      foto_del_perfil,
      descripcion,
      notificacion,
      fetched,
    } = this.state;

    // why bad request


    const { history, navigation } = this.props;
    const cerrarSesionUsuario = (cliente) => {
      AsyncStorage.removeItem('token', '');
      AsyncStorage.removeItem('id', '');

      // Desloguear
      cliente.resetStore();
      this.props.navigation.navigate('Onboarding');
    }
    let { image } = this.state;



    return (
      <Block>
           <Mutation
              mutation={ACTUALIZAR_USUARIO} >
              {(mutation) => {
                return (
                  <Query query={USER_DETAIL} variables={{ id: userId }}>
                    {(response, error, ) => {
                      if (error) {
                        console.log('Response Error-------', error);
                        return <Text style={styles.errorText}>{error}</Text>
                      }

                      if (response) {
                        const userData = response && response.data && response.data.getUsuario;
                        if (userData && !fetched) {
                          this.setUserData(userData)
                        }
                        console.log('Response-------Usuario', response);
                        return (
                          <Block>
                            <ScrollView showsHorizontalScrollIndicator={false} style={{ marginBottom: 30 }}>
                              <Block row style={{ marginBottom: 5, marginTop: 5 }}>
                                <Text bold size={18} color="#1c3643" style={{ marginTop: 15, position: 'absolute', marginLeft: 15 }}>
                                  Mis datos
                          </Text>
                                <Text size={14} color="#1c3643" style={{ marginTop: 45, position: 'absolute', marginLeft: 15, width: '60%' }}>
                                  Aqui podrás editar tu información personal
                          </Text>
                                <Block style={{ marginTop: 90, marginRight: 'auto', marginLeft: 15 }}>
                                  <Avatar
                                    onPress={this._pickImage}
                                    size='large'
                                    containerStyle={{ marginRight: 'auto', }}
                                    rounded
                                    source={{ uri: image }}
                                  />
                                </Block>
                                <ApolloConsumer>
                                  {cliente => {
                                    console.log('cliente: ', cliente)
                                    return (
                                      <Button onPress={() => Alert.prompt(
                                        'Cerrar sesión',
                                        '¿Seguro que quieres cerrar sesión?',
                                        [
                                          {
                                            text: 'Cancelar',
                                            onPress: () => console.log('Cancel Pressed'),
                                            style: 'cancel',
                                          },
                                          { text: 'Sí', onPress: () => cerrarSesionUsuario(cliente, history) },
                                        ],
                                        { cancelable: false },
                                      )} small center color="error" style={styles.optionsButton}>
                                        SALIR
                              </Button>
                                    )
                                  }}
                                </ApolloConsumer>
                              </Block>
                              <Block flex center width={width * 0.9} style={{ marginBottom: 5, marginTop: 25 }}>
                                <Input
                                  right
                                  value={nombre}
                                  onChangeText={(text) => this._handleInputValue('nombre', text)}
                                  help="Nombre"
                                  placeholder='Nombre'
                                  style={{
                                    borderColor: argonTheme.COLORS.DEFAULT,
                                    borderRadius: 4,
                                    backgroundColor: "#fff"
                                  }}
                                  iconContent={<Block />}
                                />
                              </Block>

                              <Block flex center width={width * 0.9} style={{ marginBottom: 5, marginTop: 5 }}>
                                <Input
                                  right
                                  value={apellidos}
                                  onChangeText={(text) => this._handleInputValue('apellidos', text)}
                                  help="Apellidos"
                                  placeholder="Apellidos"
                                  style={{
                                    borderColor: argonTheme.COLORS.DEFAULT,
                                    borderRadius: 4,
                                    backgroundColor: "#fff"
                                  }}
                                  iconContent={<Block />}
                                />
                              </Block>
                              <Block flex center width={width * 0.9} style={{ marginBottom: 5, marginTop: 5 }}>
                                <Input
                                  right
                                  value={profesion}
                                  onChangeText={(text) => this._handleInputValue('profesion', text)}
                                  help="Profesión"
                                  placeholder="Profesión"
                                  style={{
                                    borderColor: argonTheme.COLORS.DEFAULT,
                                    borderRadius: 4,
                                    backgroundColor: "#fff"
                                  }}
                                  iconContent={<Block />}
                                />
                              </Block>

                              <Block flex center width={width * 0.9} style={{ marginBottom: 5, marginTop: 5 }}>
                                <Input
                                  right
                                  help="Correo electrónico"
                                  placeholder="Correo electrónico"
                                  value={email}
                                  onChangeText={(text) => this._handleInputValue('email', text)}
                                  style={{
                                    borderColor: argonTheme.COLORS.DEFAULT,
                                    borderRadius: 4,
                                    backgroundColor: "#fff"
                                  }}
                                  iconContent={<Block />}
                                />
                              </Block>

                              <Block flex center width={width * 0.9} style={{ marginBottom: 5, marginTop: 5 }}>
                                <Input
                                  right
                                  help="Telefóno"
                                  value={telefono}
                                  onChangeText={(text) => this._handleInputValue('telefono', text)}
                                  placeholder="Telefono"
                                  style={{
                                    borderColor: argonTheme.COLORS.DEFAULT,
                                    borderRadius: 4,
                                    backgroundColor: "#fff"
                                  }}
                                  iconContent={<Block />}
                                />
                              </Block>

                              <Block flex center width={width * 0.9} style={{ marginBottom: 5, marginTop: 5 }}>
                                <Input
                                  right
                                  value={descripcion}
                                  onChangeText={(text) => this._handleInputValue('descripcion', text)}
                                  help="Descripción"
                                  placeholder="Descripción"
                                  style={{
                                    borderColor: argonTheme.COLORS.DEFAULT,
                                    borderRadius: 4,
                                    backgroundColor: "#fff"
                                  }}
                                  iconContent={<Block />}
                                />
                              </Block>

                              <Block
                                row
                                middle
                                space="between"
                                style={{ marginBottom: theme.SIZES.BASE, paddingHorizontal: theme.SIZES.BASE, marginTop: 30 }}
                              >
                                <Text size={14}>Permitir notificaciones</Text>
                                <Switch
                                  value={notificacion}
                                  onValueChange={() => this._handleInputValue('notificacion', !notificacion)}
                                />
                              </Block>

                              <Block center style={{ paddingHorizontal: theme.SIZES.BASE, marginTop: 30 }}>
                                <Button color="default" onPress={() => this._handleSubmit(mutation)}>
                                  GUARDAR CAMBIOS
                  </Button>
                              </Block>
                              <OpenURLButton color='primary' texto='COMPLETA TU PERFIL EN LA WEB' url={'https://locatefit.es/profile'} />
                            </ScrollView>
                          </Block>
                        )
                      }
                    }}
                  </Query>
                )
              }}
            </Mutation>
      </Block>

    );
  }
}



const styles = StyleSheet.create({
  optionsButton: {
    width: "auto",
    height: 34,
    paddingHorizontal: theme.SIZES.BASE,
    paddingVertical: 10,
    marginTop: 10,
    marginLeft: 'auto',
    marginRight: 15
  },
});



