import React from 'react';
import {
  ScrollView,
  RefreshControl,
  StyleSheet,
  Dimensions,
} from 'react-native';
import { Block, theme} from 'galio-framework';
import Loading from '../screens/loading';


const { width } = Dimensions.get('screen');

function wait(timeout) {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}

export default function App() { 
  
  const [refreshing, setRefreshing ] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    wait(2000).then(() => setRefreshing(false));
  }, [refreshing]);

  

  return (
    <Block flex center style={styles.home}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.articles}>
        <Loading />  
      </ScrollView>
      
    </Block>
  );
}

const styles = StyleSheet.create({
  home: {
    width: width,
    justifyContent: 'center'
  },
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
  categoryTitle: {
    height: "100%",
    paddingHorizontal: theme.SIZES.BASE,
    backgroundColor: "rgba(0, 0, 0, 0.3)",
    justifyContent: "center",
    alignItems: "center"
  },

  imageBlock: {
    overflow: "hidden",
    borderRadius: 4
  },
});
