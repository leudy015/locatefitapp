// local imgs
const Onboarding = require("../assets/imgs/bg.png");
const Logo = require("../assets/imgs/argon-logo.png");
const ProfileBackground = require("../assets/imgs/profile-screen-bg.png");
const RegisterBackground = require("../assets/imgs/bg-register.png");
const Pro = require("../assets/imgs/getPro-bg.png");
const ArgonLogo = require("../assets/imgs/argonlogo.png");
const iOSLogo = require("../assets/imgs/ios.png");
const androidLogo = require("../assets/imgs/android.png");
const LogoOnboarding = require('../assets/imgs/logo.png');
const Email = require('../assets/imgs/email.png');
const Astronaura = require('../assets/imgs/astronauta.png');


// internet imgs

const ProfilePicture = 'https://startupxplore.com/uploads/ff8080816d1ea989016d1fc6ceb3012e-large.png';

const Viewed = [
  'https://www.fontaneroylimpiezasvalencia.com/wp-content/uploads/2019/07/fontaneria-Valencia.jpg',
  'https://static.carrefour.es/crs/cdn-static/promo-content/Carrefour/contenidos/hogar/kit-limpieza-1.jpg',
  'https://s04.s3c.es/imag/_v0/770x420/1/5/9/700x420_empleada-limpieza-hogar-dreams.jpg',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlOhbPrls0DgRmGXHLfl3bTlVc1-cGmlj60ua5E_630oNJ3ZBkEQ&s',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlOhbPrls0DgRmGXHLfl3bTlVc1-cGmlj60ua5E_630oNJ3ZBkEQ&s',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlOhbPrls0DgRmGXHLfl3bTlVc1-cGmlj60ua5E_630oNJ3ZBkEQ&s',
];

const Products = {
  'View article': 'https://www.ikea.com/images/c4/9b/c49b590d760b63df56da02d5141901ab.jpg?f=s',
};

export default {
  Onboarding,
  Logo,
  Email,
  LogoOnboarding,
  ProfileBackground,
  ProfilePicture,
  RegisterBackground,
  Viewed,
  Products,
  Pro,
  ArgonLogo,
  iOSLogo,
  androidLogo,
  Astronaura
};