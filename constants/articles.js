export default [
  {
    title: 'Ice cream is made with carrageenan …',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlOhbPrls0DgRmGXHLfl3bTlVc1-cGmlj60ua5E_630oNJ3ZBkEQ&s',
    cta: '45€ /Hora', 
    horizontal: true
  },
  {
    title: 'Is makeup one of your daily esse …',
    image: 'https://static.carrefour.es/crs/cdn-static/promo-content/Carrefour/contenidos/hogar/kit-limpieza-1.jpg',
    cta: '25€ /Hora'
  },
  {
    title: 'Coffee is more than just a drink: It’s …',
    image: 'https://cdn.123test.com/content/beroepen/huisschilder.jpg',
    cta: '21€ /Hora' 
  },
  {
    title: 'Fashion is a popular style, especially in …',
    image: 'https://www.ikea.com/images/la-serie-de-muebles-de-pino-hemnes-incluye-armarios-altos-co-d378f9d9e23c82cdb54342d218ee22bd.jpg?f=s',
    cta: '18€ /Hora' 
  },
  {
    title: 'Argon is a great free UI packag …',
    image: 'https://www.ikea.com/images/c4/9b/c49b590d760b63df56da02d5141901ab.jpg?f=s',
    cta: '12€ /Hora', 
    horizontal: true
  },
];