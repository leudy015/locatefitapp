export default {
  COLORS: {
    DEFAULT: '#172B4D',
    PRIMARY: '#95ca3e',
    SECONDARY: '#F7FAFC',
    LABEL: '#FE2472',
    INFO: '#95ca3e',
    ERROR: '#F5365C',
    SUCCESS: '#2DCE89',
    WARNING: '#FB6340',
    /*not yet changed */
    MUTED: '#ADB5BD',
    INPUT: '#DCDCDC',
    INPUT_SUCCESS: '#7BDEB2',
    INPUT_ERROR: '#FCB3A4',
    ACTIVE: '#95ca3e', //same as primary
    BUTTON_COLOR: '#1c3643', //wtf
    PLACEHOLDER: '#9FA5AA',
    SWITCH_ON: '#95ca3e',
    SWITCH_OFF: '#D4D9DD',
    GRADIENT_START: '#1e5372',
    GRADIENT_END: '#1c3643',
    PRICE_COLOR: '#95ca3e',
    BORDER_COLOR: '#E7E7E7',
    BLOCK: '#E7E7E7',
    ICON: '#1c3643',
    HEADER: '#525F7F',
    BORDER: '#CAD1D7',
    WHITE: '#FFFFFF',
    BLACK: '#000000'
  }
};