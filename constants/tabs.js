export default tabs = {
  categories: [
    { id: 'Fontanería', title: 'Fontanería' },
    { id: 'Pintura', title: 'Pintura' },
    { id: 'Peluquería', title: 'Peluquería' },
    { id: 'Maquillaje', title: 'Maquillaje' },
    { id: 'Reparación', title: 'Reparación' },
    { id: 'Cerrajero', title:   'Cerrajero' },
    { id: 'Limpieza', title:    'Limpieza' },
  ],
}