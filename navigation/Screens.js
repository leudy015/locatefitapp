import React from "react";
import { Easing, Animated } from "react-native";
import {
  createStackNavigator,
  createDrawerNavigator,
  createAppContainer
} from "react-navigation";

import { Block } from "galio-framework";

// screens
import Home from "../screens/Home";
import Onboarding from "../screens/Onboarding";
import Pro from "../screens/Pro";
import Profile from "../screens/Profile";
import Register from "../screens/Register";
import Elements from "../screens/Elements";
import Articles from "../screens/Articles";
import Login from "../screens/Login";
import Forgot from "../screens/Forgot";
import Agenda from "../screens/Agenda";
import Payment from "../screens/Payment";
import Allproduct from "../screens/AllProduct";
import Favourites from "../screens/Favourites";
import Notification from "../screens/Notifications";
import PaymentPaypal from "../screens/PaymentPaypal";
import PaymentStripe from "../screens/PaymentStrype";
import Thank from "../screens/Thank";
import OrderDetails from "../screens/OrderDetails";
import Orders from "../screens/Orders";
import PedidoDetails from "../screens/PedidoDetails";
import Pedido from "../screens/Pedido";
import MyAcount from "../screens/MyAcount";
import MyPayment from "../screens/MyPayment";
import LinkPrivacity from "../screens/LinkPrivacity";
import ConfirmEmail from "../screens/CofirmEmail";
import Search from '../screens/Search';
import ProFavourites from '../screens/ProFavourites';
import ProfileFavourites from '../screens/ProfileFavourites';
// drawer
import Menu from "./Menu";
import DrawerItem from "../components/DrawerItem";

// header for screens
import Header from "../components/Header";

const transitionConfig = (transitionProps, prevTransitionProps) => ({
  transitionSpec: {
    duration: 400,
    easing: Easing.out(Easing.poly(4)),
    timing: Animated.timing
  },
  screenInterpolator: sceneProps => {
    const { layout, position, scene } = sceneProps;
    const thisSceneIndex = scene.index;
    const width = layout.initWidth;

    const scale = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
      outputRange: [4, 1, 1]
    });
    const opacity = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
      outputRange: [0, 1, 1]
    });
    const translateX = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex],
      outputRange: [width, 0]
    });

    const scaleWithOpacity = { opacity };
    const screenName = "Search";

    if (
      screenName === transitionProps.scene.route.routeName ||
      (prevTransitionProps &&
        screenName === prevTransitionProps.scene.route.routeName)
    ) {
      return scaleWithOpacity;
    }
    return { transform: [{ translateX }] };
  }
});



const ProfileStack = createStackNavigator(
  {
    Profilescreen: {
      screen: Profile,
      navigationOptions: ({ navigation }) => ({
        header: (
          <Header back transparent title="Perfil" navigation={navigation} />
        ),
        headerTransparent: true
      })
    }
  },
  {
    cardStyle: { backgroundColor: "#FFFFFF" },
    transitionConfig
  }
);

const ProfileFavouritesStack = createStackNavigator(
  {
    ProfilesFavouritescreen: {
      screen: ProfileFavourites,
      navigationOptions: ({ navigation }) => ({
        header: (
          <Header back transparent title="Perfil" navigation={navigation} />
        ),
        headerTransparent: true
      })
    }
  },
  {
    cardStyle: { backgroundColor: "#FFFFFF" },
    transitionConfig
  }
);

const LinkPrivacityStack = createStackNavigator(
  {
    LinkPrivacity: {
      screen: LinkPrivacity,
      navigationOptions: ({ navigation }) => ({
        header:<Header title="Información sobre tu privacidad" navigation={navigation} />
      })
    }
  },
  {
    cardStyle: {
      backgroundColor: "#F8F9FE"
    },
    transitionConfig
  }
);


const AllproductStack = createStackNavigator(
  {
    Allproductscreen: {
      screen: Allproduct,
      navigationOptions: ({ navigation }) => ({
        header: <Header search tabs={tabs.categories} title="Buscar" navigation={navigation}/>
      })
    }
  },
  {
    cardStyle: {
      backgroundColor: "#F8F9FE"
    },
    transitionConfig
  }
);

const SearchStack = createStackNavigator(
  {
    Searchscreen: {
      screen: Search,
      navigationOptions: ({ navigation }) => ({
        header: <Header search tabs={tabs.categories} title="Buscar" navigation={navigation}/>
      })
    }
  },
  {
    cardStyle: {
      backgroundColor: "#F8F9FE"
    },
    transitionConfig
  }
);

const FavouritesStack = createStackNavigator(
  {
    Favourites: {
      screen: Favourites,
      navigationOptions: ({navigation }) => ({
        header: <Header  title="Mi lista de deseos" navigation={navigation}/>
      })
    }
  },
  {
    cardStyle: {
      backgroundColor: "#F8F9FE"
    },
    transitionConfig
  }
);

const PaymentPaypalStack = createStackNavigator(
  {
    PaymentPaypal: {
      screen: PaymentPaypal,
      navigationOptions: ({navigation}) => ({
        header: <Header back  title="Pago con Paypal" navigation={navigation} />
      })
    }
  },
  {
    cardStyle: {
      backgroundColor: "#F8F9FE"
    },
    transitionConfig
  }
);

const MyPaymentStack = createStackNavigator(
  {
    MyPayment: {
      screen: MyPayment,
      navigationOptions: () => ({
        header: <Header  title="Mis datos de cobro" />
      })
    }
  },
  {
    cardStyle: {
      backgroundColor: "#F8F9FE"
    },
    transitionConfig
  }
);

const MayAcountStack = createStackNavigator(
  {
    MyAcount: {
      screen: MyAcount,
      navigationOptions: () => ({
        header: <Header  title="Mi cuenta" />
      })
    }
  },
  {
    cardStyle: {
      backgroundColor: "#F8F9FE"
    },
    transitionConfig
  }
);

const ThankStack = createStackNavigator(
  {
    Thank: {
      screen: Thank,
      navigationOptions: ({ navigation }) => ({
        header: <Header title="Gracias por tu contratación"  navigation={navigation}/>
      })
    }
  },
  {
    cardStyle: {
      backgroundColor: "#F8F9FE"
    },
    transitionConfig
  }
);

const PaymentStripeStack = createStackNavigator(
  {
    PaymentStripe: {
      screen: PaymentStripe,
      navigationOptions: ({navigation}) => ({
        header: <Header back  title="Pago con Tarjeta"  navigation={navigation}/>
      })
    }
  },
  {
    cardStyle: {
      backgroundColor: "#F8F9FE"
    },
    transitionConfig
  }
);


const OrdersStack = createStackNavigator(
  {
    Orders: {
      screen: Orders,
      navigationOptions: () => ({
        header: <Header   title="Mis Ordenes" />
      })
    }
  },
  {
    cardStyle: {
      backgroundColor: "#F8F9FE"
    },
    transitionConfig
  }
);

const PedidoStack = createStackNavigator(
  {
    Pedidoscreen: {
      screen: Pedido,
      navigationOptions: () => ({
        header: <Header   title="Mis Pedidos" />
      })
    }
  },
  {
    cardStyle: {
      backgroundColor: "#F8F9FE"
    },
    transitionConfig
  }
);

const NotificationStack = createStackNavigator(
  {
    Notification: {
      screen: Notification,
      navigationOptions: ({navigation}) => ({
        header: <Header  navigation={navigation} title="Notificaciones" />
      })
    }
  },
  {
    cardStyle: {
      backgroundColor: "#F8F9FE"
    },
    transitionConfig
  }
);

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: ({ navigation }) => ({
        header: <Header search tabs={tabs.categories} title="Locatefit" navigation={navigation} />
      })
    }
  },

  {
    cardStyle: {
      backgroundColor: "#F8F9FE"
    },
    transitionConfig
  }
);

const AgedaStack = createStackNavigator(
  {
    Agenda: {
      screen: Agenda,
      navigationOptions: ({ navigation }) => ({
        header: <Header back title="Selecciona hora y fecha"  navigation={navigation} />
      })
    }
  }
);

const PaymentStack = createStackNavigator(
  {
    Payment: {
      screen: Payment,
      navigationOptions: ({ navigation }) => ({
        header: <Header back title="Completar el pago" navigation={navigation} />
      })
    }
  }
);

// divideru se baga ca si cum ar fi un ecrna dar nu-i nimic duh
const AppStack = createDrawerNavigator(
  {
    
    Onboarding: {
      screen: Onboarding,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    Login: {
      screen: Login,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    Forgot: {
      screen: Forgot,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    ConfirmEmail: {
      screen: ConfirmEmail,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    Agenda: {
      screen: AgedaStack,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    Payment: {
      screen: PaymentStack,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    ProScreen: {
      screen: Pro,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    ProFavouritesScreen: {
      screen: ProFavourites,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    Allproduct: {
      screen: AllproductStack,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    Search: {
      screen: SearchStack,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    Favourites: {
      screen: FavouritesStack,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    Notification: {
      screen: NotificationStack,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    Profile: {
      screen: ProfileStack,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    ProFavourites: {
      screen: ProfileFavouritesStack,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    PaymentPaypal: {
      screen: PaymentPaypalStack,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    PaymentStripe: {
      screen: PaymentStripeStack,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    Thank: {
      screen: ThankStack,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    Account: {
      screen: Register,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    OrderDetailsScreen: {
      screen: OrderDetails,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    PedidoDetailsscreen: {
      screen: PedidoDetails,
      navigationOptions: navOpt => ({
        drawerLabel: () => {}
      })
    },

    Home: {
      screen: HomeStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} title="Locatefit" />
        )
      })
    },

    MyAcount: {
      screen: MayAcountStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} title="Mi Cuenta" />
        )
      })
    },

    Orders: {
      screen: OrdersStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} title="Ordenes" />
        )
      })
    },

    Pedido: {
      screen: PedidoStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} title="Pedidos" />
        )
      })
    },

    MyPayment: {
      screen: MyPaymentStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} title="Pagos" />
        )
      })
    },

    LinkPrivacity: {
      screen: LinkPrivacityStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} title="Privacidad" />
        )
      })
    },


  },
  Menu
);

const AppContainer = createAppContainer(AppStack);
export default AppContainer;
