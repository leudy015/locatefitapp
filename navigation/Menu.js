import React from "react";
import { DrawerItems } from "react-navigation";
import {
  ScrollView,
  StyleSheet,
  Dimensions,
  Image,
  AsyncStorage,
} from "react-native";
import { Block, theme, Text } from "galio-framework";
import Images from '../constants/Images';
import { Avatar } from 'react-native-elements';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../constants/config';
import { Button, Switch } from "../components/";
import SocialLogin from '../components/sociallogin';
const { width } = Dimensions.get("screen");


const USER_DETAIL = gql`
  query getUsuario($id: ID!) {
    getUsuario(id: $id) {
      email
      nombre
      apellidos
      ciudad
      telefono
      foto_del_perfil
      fotos_tu_dni
      profesion
      descripcion
      fecha_de_nacimiento
      notificacion
      grado
      estudios
      formularios_de_impuesto
      fb_enlazar
      twitter_enlazar
      instagram_enlazar
      youtube_enlazar
      propia_web_enlazar
    }
  }
`;

componentDidMount = async () => {
  const id = await AsyncStorage.getItem('id');
  console.log('este es el profeisonal id iniciada', id)
}

class Drawer extends React.PureComponent {
  state = { loggedIn: false, userId: '' }
  async componentDidMount() {
    const token = await AsyncStorage.getItem('token');
    const id = await AsyncStorage.getItem('id');
    if (token) {
      this.setState({ loggedIn: true, userId: id })
    }
  }
  async componentDidUpdate() {
    const token = await AsyncStorage.getItem('token');
    const id = await AsyncStorage.getItem('id');
    if (token) {
      this.setState({ loggedIn: true, userId: id })
    } else {
      this.setState({ loggedIn: false })
    }
  }
  render() {
    const { loggedIn, userId } = this.state;
    const { navigation } = this.props;
    if (!loggedIn) {
      return (
        <Block flex={0.05} style={styles.header1}>
          <Block>
          <Text bold size={16} color="#1c3643">
              Bienvenido
            </Text>
          </Block>
          <Block style={{width: 250, marginTop: 30}}>
          <Block center>
            <Button
              onPress={() => navigation.navigate("Login")}
              color="secondary"
              textStyle={{ color: "black", fontSize: 12, fontWeight: "700" }}
              style={styles.button}
            >
              INICIAR SESIÓN
            </Button>
          </Block>
          <Block center>
            <Button onPress={() => navigation.navigate("Account")} color="info" style={styles.button}>
              REGÍSTRARME
            </Button>

            <Text bold size={16} color="#1c3643">
              O inicia con tu redes sociales
            </Text>
            <SocialLogin />
          </Block>
          
          </Block>
        </Block>
      )
    }
    return (
      <Query query={USER_DETAIL} variables={{ id: userId }}>
        {(response, error, ) => {
          if (error) {
            console.log('Response Error-------', error);
            return <Text style={styles.errorText}>{error}</Text>
          }
          if (response) {
            const imageName = response && response.data && response.data.getUsuario.foto_del_perfil;
            const profileLink = NETWORK_INTERFACE_LINK_AVATAR + imageName;
            return (
              <Block style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
                <Block flex={0.05} style={styles.header}>
                  <Block>
                    <Avatar
                      containerStyle={{ marginRight: 'auto' }}
                      rounded
                      source={{ uri: imageName ? profileLink : Images.ProfilePicture }}
                    />
                  </Block>
                  <Block style={{ marginRight: 'auto', marginTop: 8, marginLeft: 15 }}>
                    <Text bold size={16} color="#1c3643">
                      Hola' {response && response.data ? response.data.getUsuario.nombre : ''}
                    </Text>
                  </Block>
                </Block>
                <Block flex>
                  <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
                    <DrawerItems {...this.props} />
                  </ScrollView>
                </Block>
              </Block>

            )
          }
        }}
      </Query>
    )
  }
}



const Menu = {
  contentComponent: Drawer,
  drawerBackgroundColor: "white",
  drawerWidth: width * 0.8,
  contentOptions: {
    activeTintColor: "white",
    inactiveTintColor: "#000",
    activeBackgroundColor: "transparent",
    itemStyle: {
      width: width * 0.75,
      backgroundColor: "transparent"
    },
    labelStyle: {
      fontSize: 18,
      marginLeft: 12,
      fontWeight: "normal"
    },
    itemsContainerStyle: {
      paddingVertical: 16,
      paddingHorizonal: 12,
      justifyContent: "center",
      alignContent: "center",
      alignItems: "center",
      overflow: "hidden"
    }
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    marginLeft: 28,
    marginBottom: theme.SIZES.BASE,
    marginTop: theme.SIZES.BASE * 3,
    flexDirection: 'row',
  },
  header1: {
    marginLeft: 28,
    marginBottom: theme.SIZES.BASE,
    marginTop: theme.SIZES.BASE * 6,
  },
  button: {
    marginBottom: theme.SIZES.BASE,
    width: 250
  },
});

export default Menu;
