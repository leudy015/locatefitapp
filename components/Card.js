import React from 'react';
import { withNavigation } from 'react-navigation';
import PropTypes from 'prop-types';
import { StyleSheet, Dimensions, Image, TouchableWithoutFeedback } from 'react-native';
import { Block, Text, theme } from 'galio-framework';
import { Rating, Avatar } from 'react-native-elements';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { argonTheme, Images } from '../constants';

const CATEGORY_QUERY = gql`
  query getCategory($id: ID!, $price: Int, $city: String) {
    getCategory(id: $id, price: $price, city: $city) {
      success
      message
      data {
        id
        title
        productos {
          id
          city
          category_id
          currency
          description

          domingo
          domingo_from
          domingo_to

          jueves
          jueves_from
          jueves_to

          lunes
          lunes_from
          lunes_to

          martes
          martes_from
          martes_to

          miercoles
          miercoles_from
          miercoles_to

          number

          sabado
          sabado_from
          sabado_to

          time
          title

          viernes
          viernes_from
          viernes_to

          fileList

          anadidoFavorito

          created_by

          category {
            id
            title
          }

          creator {
            id
            usuario
            email
            nombre
            apellidos
            ciudad
            telefono
            tipo
            foto_del_perfil
            fotos_tu_dni
            profesion
            descripcion
            fecha_de_nacimiento
            notificacion
            grado
            estudios
            formularios_de_impuesto
            fb_enlazar
            twitter_enlazar
            instagram_enlazar
            youtube_enlazar
            propia_web_enlazar
          }
        }
      }
    }
  }
`


class Card extends React.Component {
  render() {
    const { navigation, item, horizontal, full, style, ctaColor, imageStyle } = this.props;

    const imageStyles = [
      full ? styles.fullImage : styles.horizontalImage,
      imageStyle
    ];
    const cardContainer = [styles.card, styles.shadow, style];
    const imgContainer = [styles.imageContainer,
    horizontal ? styles.horizontalStyles : styles.verticalStyles,
    styles.shadow
    ];

    const { data } = this.props;
   
    return (
      <Block row={horizontal} card flex style={cardContainer}>
        <TouchableWithoutFeedback onPress={() => navigation.navigate('Pro')}>
          <Block flex style={imgContainer}>
            <Image source={{ uri: item.image }} style={imageStyles} />
          </Block>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={() => navigation.navigate('Pro')}>
          <Block flex space="between" style={styles.cardDescription}>
            <Text size={14} style={styles.cardTitle}>{item.title}</Text>
            <Block left style={{ marginBottom: 7 }}>
              <Text size={12} muted={!ctaColor} color={ctaColor || argonTheme.COLORS.DEFAULT} bold>BURGOS</Text>
            </Block>
            <Block left style={{ marginBottom: 7 }}>
              <Rating
                imageSize={10}
                readonly
                startingValue={5}
                style={{ marginLeft: 0 }}
              />
            </Block>
            <Block style={{ flexDirection: 'row' }}>
              <Text size={16} muted={!ctaColor} color={ctaColor || argonTheme.COLORS.ACTIVE} bold>{item.cta}</Text>
              <Avatar
                containerStyle={{marginLeft: 'auto', marginTop: -10 }}
                rounded
                source={{ uri: Images.ProfilePicture }}
              />
            </Block>
          </Block>
        </TouchableWithoutFeedback>
      </Block>
    );
  }
}

Card.propTypes = {
  item: PropTypes.object,
  horizontal: PropTypes.bool,
  full: PropTypes.bool,
  ctaColor: PropTypes.string,
  imageStyle: PropTypes.any,
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: theme.COLORS.WHITE,
    marginVertical: theme.SIZES.BASE,
    borderWidth: 0,
    minHeight: 114,
    marginBottom: 16
  },
  cardTitle: {
    flex: 1,
    flexWrap: 'wrap',
    paddingBottom: 6
  },
  cardDescription: {
    padding: theme.SIZES.BASE / 2
  },
  imageContainer: {
    borderRadius: 3,
    elevation: 1,
    overflow: 'hidden',
  },
  image: {
    // borderRadius: 3,
  },
  horizontalImage: {
    height: 122,
    width: 'auto',
  },
  horizontalStyles: {
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  },
  verticalStyles: {
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0
  },
  fullImage: {
    height: 215
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    shadowOpacity: 0.1,
    elevation: 2,
  },
});





export default withNavigation(Card);