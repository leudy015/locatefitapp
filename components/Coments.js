import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Rating} from 'react-native-elements';
import Icon from '../components/Icon';



export default class Coments extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{ width: 280, height: 'auto', margin: 10, backgroundColor: "#F3F3F3", color: "464646", padding: 10, borderRadius: 10, }}>
          <View style={{ width: '100%', padding: 5, flexDirection: 'row', }}>
            <View>
              <Text style={{ fontSize: 20, marginLeft: 5, fontWeight: '300', color: '#95ca3e', }}>
                Leudy Martes
                    </Text>
              <Text style={{ fontSize: 12, marginLeft: 5, fontWeight: '300', color: '#333', }}>
                Burgos
                    </Text>
            </View>
        
            <Text style={{ fontSize: 12, fontWeight: '400', color: '#BDBDBD', marginLeft: 'auto', alignItems: 'flex-end', marginTop: 5 }}>
            <Icon family="Font-Awesome" name="star" size={18} color="#FFCE4E" /> (4.6) Valoración
            </Text>
          </View>
          <Text style={{ fontSize: 14, fontWeight: '400', color: '#333', textAlign: "justify", padding: 15 }}>
            Lorem Ipsum es simplemente un texto ficticio de la industria de impresión y
            composición tipográfica. Lorem Ipsum ha sido el texto ficticio estándar de la
            industria desde el año 1500, cuando una impresora desconocida tomó una galera
                </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
});