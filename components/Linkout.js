'use strict';
import { Button } from '../components';
var React = require('react');
var PropTypes = require('prop-types');
var ReactNative = require('react-native');
import { Block } from 'galio-framework';
var { Linking } = ReactNative;

export default class OpenURLButton extends React.Component {
    static propTypes = {
        url: PropTypes.string,
    };

    handleClick = () => {
        Linking.canOpenURL(this.props.url).then(supported => {
            if (supported) {
                Linking.openURL(this.props.url);
            } else {
                console.log("Don't know how to open URI: " + this.props.url);
            }
        });
    };

    render() {
        return (
            <Block center style={{ marginTop: 15, marginBottom: 15 }}>
                <Button onPress={this.handleClick} color={this.props.color} >
                    {this.props.texto}
                </Button>
            </Block>

        );
    }
}
