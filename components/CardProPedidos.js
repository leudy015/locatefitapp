import React, { PureComponent } from 'react';
import { StyleSheet, Dimensions, Image, TouchableWithoutFeedback, FlatList, ActivityIndicator, AsyncStorage } from 'react-native';
import PropTypes from 'prop-types';
import { Block, Text, theme, Button } from 'galio-framework';
import { Rating } from 'react-native-elements';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { argonTheme, Images } from '../constants';
import NavigationServices from '../navigation/Services'
import { NETWORK_INTERFACE_LINK } from '../constants/config';


class CardOrdenes extends PureComponent {

  constructor(props) {
    super(props);
    console.log('this.props in tableordenes: ', this.props)
    this.state = {
      ordenes: [],
      dataLoading: true
    };
  }

  refetch = null;

  _renderItem({ item }) {

    const { navigation, horizontal, full, style, ctaColor, imageStyle } = this.props;
    const imageStyles = [full ? styles.fullImage : styles.horizontalImage, imageStyle];
    const cardContainer = [styles.card, styles.shadow, style];
    const imgContainer = [styles.imageContainer, horizontal ? styles.horizontalStyles : styles.verticalStyles, styles.shadow];

    return (
      <Block row={horizontal} card flex style={cardContainer}>
        <TouchableWithoutFeedback onPress={() => NavigationServices.navigate('PedidoDetailsscreen', { data: item })}>
          <Block flex style={imgContainer}>
            {item.product.fileList.length > 0 ?
              <Image source={{ uri: item.product.fileList.length > 0 ? NETWORK_INTERFACE_LINK + "/assets/images/" + item.product.fileList[0] : "" }} style={imageStyles} /> : null}
          </Block>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={() => NavigationServices.navigate('PedidoDetailsscreen', { data: item })}>
          <Block flex space="between" style={styles.cardDescription}>
            <Text size={14} style={styles.cardTitle}>{item.product.title}</Text>
            <Block left style={{ marginBottom: 7 }}>
              <Text size={16} muted={!ctaColor} color={ctaColor || argonTheme.COLORS.ACTIVE} bold>{item.product.number + '€' + item.product.currency}</Text>
            </Block>
            <Block left style={{ marginBottom: 7 }}>
              <Rating
                imageSize={10}
                readonly
                startingValue={5}
                style={{ marginLeft: 0 }}
              />
            </Block>
            <Block style={{ flexDirection: 'row' }}>
              <Text size={12} muted={!ctaColor} color={ctaColor || argonTheme.COLORS.DEFAULT} bold>{item.product.city}</Text>
              <Text size={12} style={styles.estados}>{item.estado}</Text>
            </Block>
          </Block>
        </TouchableWithoutFeedback>
      </Block>
    );
  }

  formatResult = data => {
    if (data && data.getPedidosByCliente && data.getPedidosByCliente.success) {
      this.setState({
        dataLoading: false,
        ordenes: data.getPedidosByCliente.list
      });
      console.log(data.getPedidosByCliente.list)
    }
  }

  componentDidMount = async () => {
    const cliente = await AsyncStorage.getItem('id');
    this.setState({ cliente });
  }

  render() {
    const { cliente } = this.state;
    console.log('dfdsfdsf', cliente)
    return (
      <Query query={CLIENTE_PEDIDO_QUERY} variables={{ cliente }} onCompleted={this.formatResult}>
        {(response, error, loading, refetch) => {
            this.refetch = refetch;
          if (loading) {
            return <ActivityIndicator size="large" color="#95ca3e" />
          }

          if (error) {
            console.log('Response Error-------', error);
            return <Text style={styles.errorText}>{error}</Text>
          }

          if (response) {
            return <FlatList
              data={this.state.ordenes}
              showsHorizontalScrollIndicator={false}
              renderItem={(item) => this._renderItem(item, response.refetch)}
              keyExtractor={item => item.id}
              ListEmptyComponent={<Block center style={{ marginTop: 50 }}>
                <Image source={Images.Astronaura} style={{ width: 150, height: 150, marginBottom: 40 }} />
                <Block center style={{ marginBottom: 30 }}>
                  <Text style={{ color: argonTheme.COLORS.PLACEHOLDER, textAlign: 'center' }}>Aún no has realizado ningún pedido, explora entre todo los profeisnales y empieza a contratar.</Text>
                </Block>

                <Block center>
                  <Button onPress={() => navigation.navigate('Search')} color="primary" style={styles.button}>
                    EMPEZAR A CONTRATAR
                  </Button>
                </Block>
              </Block>}
            />
          }
        }}
      </Query>
    );
  }
}

CardOrdenes.propTypes = {
  item: PropTypes.object,
  horizontal: PropTypes.bool,
  full: PropTypes.bool,
  ctaColor: PropTypes.string,
  imageStyle: PropTypes.any,
}


const styles = StyleSheet.create({
  card: {
    backgroundColor: theme.COLORS.WHITE,
    marginVertical: theme.SIZES.BASE,
    borderWidth: 0,
    minHeight: 114,
    marginBottom: 10
  },

  estados: {
    marginLeft: 'auto',
    padding: 5,
    backgroundColor: argonTheme.COLORS.INPUT_SUCCESS,
    color: argonTheme.COLORS.WHITE,
    borderRadius: 15
  },

  cardTitle: {
    flex: 1,
    flexWrap: 'wrap',
    paddingBottom: 6
  },
  cardDescription: {
    padding: theme.SIZES.BASE / 2
  },
  imageContainer: {
    borderRadius: 3,
    elevation: 1,
    overflow: 'hidden',
  },
  image: {
    // borderRadius: 3,
  },
  horizontalImage: {
    height: 122,
    width: 'auto',
  },
  horizontalStyles: {
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  },
  verticalStyles: {
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0
  },
  fullImage: {
    height: 215
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    shadowOpacity: 0.1,
    elevation: 2,
  },

  spinner: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const CLIENTE_PEDIDO_QUERY = gql`
      query 
        getPedidosByCliente($cliente: ID!, $dateRange: DateRangeInput) {
          getPedidosByCliente(cliente: $cliente, dateRange: $dateRange) {
            success
            message
            list {
              id
              cupon
              nota
              aceptaTerminos
              producto
              cantidad
              cliente
              estado
              endDate
              time
              progreso
              status
              created_at
              product {
                id
                title 
                number
                currency
                fileList
                time
              }
              pagoPaypal {
                idPago
                idPagador
                fechaCreacion
                emailPagador
                nombrePagador
                apellidoPagador
                estadoPago
                moneda
                montoPagado
              }
              prfsnl {
                id
                nombre
                apellidos
                profesion
                notificacion
                telefono
                foto_del_perfil
                fotos_tu_dni
                fecha_de_nacimiento
                ciudad
                descripcion
                email
                grado
                usuario
                tipo
                }
              }
            }
          }
        `;

export default CardOrdenes;