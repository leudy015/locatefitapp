import React from 'react';
import { SocialIcon } from 'react-native-elements'
import { Block } from "galio-framework";



class SocialButton extends React.Component {
    render() {
        return (
            <Block row style={{ marginTop: 10 }}>
                <SocialIcon
                    title='Inicia sesión con Facebook'
                    type='facebook'
                />

                <SocialIcon
                    title='Inicia sesión con Twitter'
                    type='twitter'
                />

                <SocialIcon
                    title='Inicia sesión con Google'
                    type='google'
                />
            </Block>

        )
    }
}

export default SocialButton;