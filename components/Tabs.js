import React from 'react';
import { StyleSheet, Dimensions, FlatList, Animated, TouchableOpacity } from 'react-native';
import { Block, theme, Text} from 'galio-framework';
const { width } = Dimensions.get('screen');
import argonTheme from '../constants/Theme';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';


const categories = gql`
    query {
      getCategories {
        id
        title
      }
    }
`

class Tabs extends React.Component {
  
  renderItem = (item) => {
    const containerStyles = [
      styles.titleContainer
    ];
    const { navigation } = this.props;
    return (
      <Block style={containerStyles}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Allproductscreen', { data: item })}>
          <Text style={styles.menuTitle} >{item.title}</Text>
        </TouchableOpacity>
      </Block>
    )
  }

  renderMenu = () => {
    return (
      <Query query={categories}>
        {(response, error, loading) => {
          if (loading) {
            return (<View style={styles.spinner}>
              <ActivityIndicator size="large" color="#95ca3e" />
            </View>)
          }
          if (error) {
            console.log('Response Error-------', error);
            return <Text style={styles.errorText}>{error}</Text>
          }
          if (response) {
            console.log('response-data-------------', response);
            return (<FlatList
              data={response && response.data ? response.data.getCategories : ''}
              horizontal={true}
              extraData={this.state}
              keyExtractor={(item) => item.id}
              showsHorizontalScrollIndicator={false}
              renderItem={({ item }) => this.renderItem(item)}
              contentContainerStyle={styles.menu}
            />);
          }
        }}
      </Query>

    )
  }

  render() {
    return (
      <Block style={styles.container}>
        {this.renderMenu()}
      </Block>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: width,
    backgroundColor: theme.COLORS.WHITE,
    zIndex: 2,
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4,
  },
  menu: {
    marginLeft: 15,
    paddingTop: 8,
    paddingBottom: 16,
  },
  titleContainer: {
    alignItems: 'center',
    backgroundColor: argonTheme.COLORS.ACTIVE,
    borderRadius: 5,
    marginRight: 9,
  },
  containerShadow: {
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    shadowOpacity: 0.1,
    elevation: 1,
  },
  menuTitle: {
    fontWeight: '600',
    fontSize: 14,
    // lineHeight: 28,
    paddingVertical: 10,
    paddingHorizontal: 16,
    color: argonTheme.COLORS.WHITE
  },
});



export default Tabs;
