import React, { PureComponent } from 'react';
import { StyleSheet, Dimensions, Image, TouchableWithoutFeedback, FlatList, ActivityIndicator} from 'react-native';
import PropTypes from 'prop-types';
import { Block, Text, theme } from 'galio-framework';
import { Rating, Avatar } from 'react-native-elements';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { argonTheme, Images } from '../constants';
import NavigationServices from '../navigation/Services'
import { NETWORK_INTERFACE_LINK } from '../constants/config';
import {
  Placeholder,
  PlaceholderMedia,
  PlaceholderLine,
  Fade
} from "rn-placeholder";

const ProdcutoListQuery = gql`
query{
    getPro {
         id
         city
         category_id
         currency
         description
         domingo
         domingo_from
         domingo_to
         jueves
         jueves_from
         jueves_to
         lunes
         lunes_from
         lunes_to
         martes
         martes_from
         martes_to
         miercoles
         miercoles_from
         miercoles_to
         number
         sabado
         sabado_from
         sabado_to
         time
         title
         viernes
         visitas
         viernes_from
         viernes_to
         fileList
         created_by
         category {
           id
           title
         }
         creator {
           id
           usuario
           email
           nombre
           apellidos
           ciudad
           telefono
           tipo
           foto_del_perfil
           fotos_tu_dni
           profesion
           descripcion
           fecha_de_nacimiento
           notificacion
           grado
           estudios
           formularios_de_impuesto
           fb_enlazar
           twitter_enlazar
           instagram_enlazar
           youtube_enlazar
           propia_web_enlazar
         }
       }
   }
`


class PlayersList extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      
    }
  }

    _renderItem({ item }) {
      
        const { navigation, horizontal, full, style, ctaColor, imageStyle } = this.props;
        const imageStyles = [full ? styles.fullImage : styles.horizontalImage, imageStyle];
        const cardContainer = [styles.card, styles.shadow, style];
        const imgContainer = [styles.imageContainer, horizontal ? styles.horizontalStyles : styles.verticalStyles, styles.shadow];

        return (  
            <Block row={horizontal} card flex style={cardContainer}>
                <TouchableWithoutFeedback onPress={() => NavigationServices.navigate('ProScreen', { data: item })}>
                    <Block flex style={imgContainer}>
                    {item.fileList.length > 0 ?
                        <Image source={{ uri: item.fileList.length > 0 ?  NETWORK_INTERFACE_LINK + "/assets/images/" + item.fileList[0] : "" }} style={imageStyles} /> : null}
                    </Block>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => NavigationServices.navigate('ProScreen', { data: item  })}>
                    <Block flex space="between" style={styles.cardDescription}>
                        <Text size={14} style={styles.cardTitle}>{item.title}</Text>
                        <Block left style={{ marginBottom: 7 }}>
                            <Text size={12} muted={!ctaColor} color={ctaColor || argonTheme.COLORS.DEFAULT} bold>{item.city}</Text>
                        </Block>
                        <Block left style={{ marginBottom: 7 }}>
                            <Rating
                                imageSize={10}
                                readonly
                                startingValue={5}
                                style={{ marginLeft: 0 }}
                            />
                        </Block>
                        <Block style={{ flexDirection: 'row' }}>
                            <Text size={16} muted={!ctaColor} color={ctaColor || argonTheme.COLORS.ACTIVE} bold>{item.number + '€' + item.currency}</Text>
                            <Avatar
                                containerStyle={{ marginLeft: 'auto', marginTop: -10 }}
                                rounded
                                source={{ uri: NETWORK_INTERFACE_LINK + "/assets/images/" + item.creator.foto_del_perfil }}
                            />
                        </Block>
                    </Block>
                </TouchableWithoutFeedback>
            </Block>
        );
    }

    render() {
          const { navigation } = this.props;
        return (
            <Query query={ProdcutoListQuery}>
                {(response, error, loading ) => {
                  
                  if (loading) { 
                    return  <ActivityIndicator size="large" color="#95ca3e" />
                  }
                     
                    if (error) {
                        console.log('Response Error-------', error);
                        return <Text style={styles.errorText}>{error}</Text>
                    }  
                  
                    if (response) {
                        console.log('response-data-------------', response);
                        return <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={item => item.id}
                            data={response && response.data ? response.data.getPro : ''}
                            renderItem={(item) => this._renderItem(item)}
                        />;
                    }
                }}
            </Query>
        );
    }
}

PlayersList.propTypes = {
    item: PropTypes.object,
    horizontal: PropTypes.bool,
    full: PropTypes.bool,
    ctaColor: PropTypes.string,
    imageStyle: PropTypes.any,
  }

  const styles = StyleSheet.create({
    card: {
      backgroundColor: theme.COLORS.WHITE,
      marginVertical: theme.SIZES.BASE,
      borderWidth: 0,
      minHeight: 114,
      width: 210,
      marginBottom: 16
    },
    cardTitle: {
      flex: 1,
      flexWrap: 'wrap',
      paddingBottom: 6
    },
    cardDescription: {
      padding: theme.SIZES.BASE / 2
    },
    imageContainer: {
      borderRadius: 3,
      elevation: 1,
      overflow: 'hidden',
    },
    image: {
      // borderRadius: 3,
    },
    horizontalImage: {
      height: 122,
      width: 'auto',
    },
    horizontalStyles: {
      borderTopRightRadius: 0,
      borderBottomRightRadius: 0,
    },
    verticalStyles: {
      borderBottomRightRadius: 0,
      borderBottomLeftRadius: 0
    },
    fullImage: {
      height: 215
    },
    shadow: {
      shadowColor: theme.COLORS.BLACK,
      shadowOffset: { width: 0, height: 2 },
      shadowRadius: 4,
      shadowOpacity: 0.1,
      elevation: 2,
    },

    spinner: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      },
  });

export default PlayersList;