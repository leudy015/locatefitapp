import React from 'react';
import { Image , AsyncStorage} from 'react-native';
import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import { Block, GalioProvider } from 'galio-framework';

import Screens from './navigation/Screens';
import NavigationServices from './navigation/Services'
import { Images, articles, argonTheme } from './constants';

import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
// import { HttpLink } from 'apollo-link-http';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { NETWORK_INTERFACE } from './constants/config';

// cache app images
const assetImages = [
  Images.Onboarding,
  Images.LogoOnboarding,
  Images.Logo,
  Images.Pro,
  Images.ArgonLogo,
  Images.iOSLogo,
  Images.androidLogo
];

// cache product images
articles.map(article => assetImages.push(article.image));

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

const httpLink = createHttpLink({
  uri: NETWORK_INTERFACE,
});

const authLink = setContext(async (_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = await AsyncStorage.getItem('token');
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? token : "",
    }
  }
});

const apolloClient = new ApolloClient({
  // link: new HttpLink({ uri: NETWORK_INTERFACE, credentials: 'include'}),
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
})
export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  }
  
  render() {
    if(!this.state.isLoadingComplete) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <GalioProvider theme={argonTheme}>
          <Block flex>
            <ApolloProvider client={apolloClient}>
              <Screens  ref={navigatorRef => {
              NavigationServices.setTopLevelNavigator(navigatorRef);
            }}/>
            </ApolloProvider>
          </Block>
        </GalioProvider>
      );
    }
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      ...cacheImages(assetImages),
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };

}
